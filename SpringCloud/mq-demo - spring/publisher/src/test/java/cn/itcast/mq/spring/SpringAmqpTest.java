package cn.itcast.mq.spring;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringAmqpTest {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    public void testSimpleQueue() throws InterruptedException {
        String queueName = "simple.queue";
        String message = "hello , spring__";
        for (int i = 0; i < 50; i++) { //模拟每秒接受50条消息
            rabbitTemplate.convertAndSend(queueName, message + i);
            Thread.sleep(20);
        }
    }


    @Test
    public void testSendFanoutExchange() {
        //交换机名称
        String exchangeName = "mannor.fanout";
        //消息
        String message = "hello,every queue!";
        //发送
        rabbitTemplate.convertAndSend(exchangeName, "", message);
    }

    @Test
    public void testSendDirectExchange() {
        //交换机名称
        String exchangeName = "mannor.direct";
        //消息
        String message = "hello,every queue!";
        //发送
        rabbitTemplate.convertAndSend(exchangeName, "blue", message);
    }

    @Test
    public void testSendTopicExchange() {
        //交换机名称
        String exchangeName = "mannor.topic";
        //消息
        String message = "曼诺尔雷迪亚兹入驻抖音啦！";
        //发送
        rabbitTemplate.convertAndSend(exchangeName, "china1.news", message);
    }


    @Test
    public void testSendObjectQueue() {
        Map<String, Object> obj = new HashMap<>();
        obj.put("姓名", "曼诺尔雷迪亚兹");
        obj.put("性别", "男");
        obj.put("年龄", "45");
        rabbitTemplate.convertAndSend("object.queue", obj);
    }

}
