
package cn.mannor.hotel;

import org.apache.http.HttpHost;
import org.apache.ibatis.annotations.Delete;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static cn.mannor.hotel.constants.HotelConstants.MAPPING_TEMPLATE;

public class HotelIndexTest {

    private RestHighLevelClient restHighLevelClient; // 高级REST客户端，用于与Elasticsearch进行交互

    @Test
    void testInte() {
        System.out.println(restHighLevelClient);
    }

    @Test
    void createHotelIndex() throws IOException {
        //1.创建Request参数
        CreateIndexRequest request = new CreateIndexRequest("hotel");//传入索引名称
        //2.准备请求的参数,DSL语句
        request.source(MAPPING_TEMPLATE, XContentType.JSON);
        //3.发送请求，indices返回库中包含的所有索引库。
        restHighLevelClient.indices().create(request, RequestOptions.DEFAULT);
    }

    @Test
    void deleteHotelIndex() throws IOException {
        //1.创建Request参数
        DeleteIndexRequest request = new DeleteIndexRequest("hotel");//传入索引名称
        //2.发送请求，indices返回库中包含的所有索引库。
        restHighLevelClient.indices().delete(request, RequestOptions.DEFAULT);
    }

    @Test
    void exitsHotelIndex() throws IOException {
        //1.创建Request参数
        GetIndexRequest request = new GetIndexRequest("hotel");//传入索引名称
        //2.发送请求，indices返回库中包含的所有索引库。
        boolean exists = restHighLevelClient.indices().exists(request, RequestOptions.DEFAULT);
        //3.输出结果
        System.out.println(exists?"存在该索引！":"不存在该索引！");
    }



    /**
     * 初始化测试环境，创建并配置RestHighLevelClient实例。
     */
    @BeforeEach
    void setUp() {
        this.restHighLevelClient = new RestHighLevelClient(RestClient.builder(
                HttpHost.create("http://192.168.12.131:9200") // 设置Elasticsearch的地址和端口
        ));
    }

    /**
     * 测试结束后清理资源，关闭RestHighLevelClient实例。
     *
     * @throws IOException 如果关闭客户端时发生IO错误
     */    @AfterEach

    void tearDown() throws IOException {
        this.restHighLevelClient.close(); // 关闭高级REST客户端，释放资源
    }
}