
package cn.mannor.hotel;

import cn.mannor.hotel.pojo.Hotel;
import cn.mannor.hotel.pojo.HotelDoc;
import cn.mannor.hotel.service.IHotelService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpHost;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.List;

import static cn.mannor.hotel.constants.HotelConstants.MAPPING_TEMPLATE;

@SpringBootTest
public class HotelDocumentTest {

    private RestHighLevelClient restHighLevelClient; // 高级REST客户端，用于与Elasticsearch进行交互

    @Autowired
    private IHotelService iHotelService;

    @Test
    void addDocumentTest() throws IOException {
        Hotel hotel = iHotelService.getById(45845L); //获取数据
        HotelDoc hotelDoc = new HotelDoc(hotel);//处理文档中不同的数据类型
        //1.准备request对象
        IndexRequest request = new IndexRequest("hotel").id(hotelDoc.getId().toString());
        //2.准备JOSN文档(由JSON转换过来)
        request.source(JSON.toJSONString(hotelDoc), XContentType.JSON);
        //3.发送请求
        restHighLevelClient.index(request, RequestOptions.DEFAULT);
    }


    @Test
    void getDocumentByIdTest() throws IOException {

        //1.准备request对象
        GetRequest request = new GetRequest("hotel", "45845");

        //2.发送请求,得到响应
        GetResponse response = restHighLevelClient.get(request, RequestOptions.DEFAULT);
        //3.解析响应结果
        String json = response.getSourceAsString();
        HotelDoc hotelDoc = JSON.parseObject(json, HotelDoc.class);
        System.out.println(hotelDoc);
    }

    @Test
    void updateDocumentByIdTest() throws IOException {

        //1.准备request对象
        UpdateRequest request = new UpdateRequest("hotel", "45845");
        //2.准备文档，键值对形式
        request.doc(
                "age", 18,
                "name", "曼诺尔雷迪亚兹"
        );
        //3.发送请求
        restHighLevelClient.update(request, RequestOptions.DEFAULT);
    }

    @Test
    void deleteDocumentByIdTest() throws IOException {

        //1.准备request对象
        DeleteRequest request = new DeleteRequest("hotel", "45845");

        //2.发送请求
        restHighLevelClient.delete(request, RequestOptions.DEFAULT);
    }


    @Test
    void bulkDocumentTest() throws IOException {

        List<Hotel> hotels = iHotelService.list();
        //1.准备request对象
        BulkRequest request = new BulkRequest();
        //2.准备参数，添加多个Request
        for (Hotel hotel : hotels) {
            HotelDoc hotelDoc = new HotelDoc(hotel);
            request.add(new IndexRequest("hotel")
                    .id(hotelDoc.getId().toString())
                    .source(JSON.toJSONString(hotelDoc), XContentType.JSON));
        }
        //3.发送请求
        restHighLevelClient.bulk(request, RequestOptions.DEFAULT);
    }


    /**
     * 初始化测试环境，创建并配置RestHighLevelClient实例。
     */
    @BeforeEach
    void setUp() {
        this.restHighLevelClient = new RestHighLevelClient(RestClient.builder(
                HttpHost.create("http://192.168.12.131:9200") // 设置Elasticsearch的地址和端口
        ));
    }

    /**
     * 测试结束后清理资源，关闭RestHighLevelClient实例。
     *
     * @throws IOException 如果关闭客户端时发生IO错误
     */
    @AfterEach
    void tearDown() throws IOException {
        this.restHighLevelClient.close(); // 关闭高级REST客户端，释放资源
    }
}