package cn.mannor.hotel.service.impl;

import cn.mannor.hotel.mapper.HotelMapper;
import cn.mannor.hotel.pojo.Hotel;
import cn.mannor.hotel.service.IHotelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class HotelService extends ServiceImpl<HotelMapper, Hotel> implements IHotelService {
}
