package cn.mannor.hotel.mapper;

import cn.mannor.hotel.pojo.Hotel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface HotelMapper extends BaseMapper<Hotel> {
}
