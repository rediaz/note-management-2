# MQ学习

[TOC]



## 1、初识MQ

学习mq之前，就要先知道同步通讯和异步通讯的区别。

### 1.1、同步通讯和异步通讯

![image-20240305173222748](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240305173222748.png)

- 同步通讯就像是打电话，就如上面的图片一样，一次只能拨打一个人的电话，否则就是占线的状态。

+ 异步通讯就好比你同时可以接受微信多个用户的消息，就算你人不在线也会有消息的内容。

### 1.2、同步调用存在的问题

微服务间基于Feign的调用就属于同步方式，存在一些问题。

1. 耦合度高

   每次加入新的需求，都要修改原来的代码。

2. 性能下降

   调用者需要等待服务提供者响应，如果调用链过长则响应时间等于每次调用的时间之和。

3. 资源浪费

   调用链中的每个服务在等待响应过程中，不能释放请求占用的资源，高并发场景下会极度浪费系统资源。

4. 级联失败

   如果服务提供者出现问题，所有调用方都会跟着出问题，如同多米诺骨牌一样，迅速导致整个微服务群故障。

### 1.3、异步调用方案

异步调用常见实现就是事件驱动模式

![image-20240305174006119](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240305174006119.png)

可以解决多个问题：

优势一：服务解耦

![image-20240305174135917](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240305174135917.png)

优势二：性能提升，吞吐量提高

![image-20240305174147651](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240305174147651.png)

优势三：服务没有强依赖，不担心级联失败问题

![image-20240305174155177](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240305174155177.png)

优势四：流量削峰（将待处理的消息存在消息队列中，一个一个进行处理）

![image-20240305174054584](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240305174054584.png)



### 1.4、异步通信的缺点

+ 依赖于Broker的可靠性、安全性、吞吐能力

+ 架构复杂了，业务没有明显的流程线，不好追踪管理

> 事实上，我们大多数情况都会使用同步通信，因为大多数情况下对并发的要求不是很高，要求较高的是时效性。
>
> 而异步通通信就是上述的那些概念：并发，解耦等。

## 2、RabbitMQ快速入门

### 2.1、什么是MQ？

- **MQ （MessageQueue）**，中文是**消息队列**，字面来看就是存放消息的队列。也就是事件驱动架构中的Broker。

- **消息**：就是一个个待处理的事件。

常见的MQ中间件：

![image-20240305175616416](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240305175616416.png)



### 2.2、RabbitMQ概述

**RabbitMQ**是基于**Erlang**语言开发的开源**消息通信中间件**，官网地址：[https://www.rabbitmq.com/](https://www.rabbitmq.com/)

RabbitMQ安装参考文档：[RabbitMQ部署指南](https://gitee.com/rediaz/note-management-2/blob/master/SpringCloud/itheima%E5%AE%9E%E7%94%A8%E6%96%87%E6%A1%A3/RabbitMQ%E9%83%A8%E7%BD%B2%E6%8C%87%E5%8D%97.md)

### 2.3、RabbitMQ的结构和概念

![image-20240305205451729](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240305205451729.png)

RabbitMQ中的几个概念：

- **channel**：操作MQ的工具

- **exchange**：路由消息到队列中

- **queue**：缓存消息

- **virtual host**：虚拟主机，是对queue、exchange等资源的逻辑分组

### 3.4、常见消息模型

MQ的官方文档中给出了5个MQ的Demo示例，对应了几种不同的用法：

+ 基本消息队列（BasicQueue）

  ![image-20240305210209074](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240305210209074.png)

+ 工作消息队列（WorkQueue）

  ![image-20240305210212626](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240305210212626.png)

+ 发布订阅（Publish、Subscribe），又根据交换机类型不同分为三种：

  + Fanout Exchange：广播

    ![image-20240305210216601](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240305210216601.png)

  + Direct Exchange：路由

    ![image-20240305210224154](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240305210224154.png)

  + Topic Exchange：主题

    ![image-20240305210229106](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240305210229106.png)

### 3.5、HelloWorld

官方的HelloWorld是基于最基础的消息队列模型来实现的，只包括三个角色:

-  publisher：消息发布者，将消息发送到队列queue
-  queue：消息队列，负责接受并缓存消息
-  consumer：订阅队列，处理队列中的消息

[测试demo](https://gitee.com/rediaz/note-management-2/tree/master/SpringCloud/mq-demo)：有详细的注释，访问RabbitMQ管理端口来断点测试，感受消息队列的流程。

![image-20240305211815608](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240305211815608.png)

基本消息队列的消息**发送**流程：

1. 建立connection

2. 创建channel

3. 利用channel声明队列

4. 利用channel向队列发送消息

基本消息队列的消息**接收**流程：

1. 建立connection

2. 创建channel

3. 利用channel声明队列

4. 定义consumer的消费行为handleDelivery()

5. 利用channel将消费者与队列绑定

## 3、SpringAMQP

### 3.1、什么是SpringAMQP？

SpringAmqp的官方地址：https://spring.io/projects/spring-amqp

![image-20240305212913808](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240305212913808.png)

### 3.2、利用SpringAMQP实现HelloWorld中的基础消息队列功能

**发送：**

步骤1：引入AMQP依赖

因为publisher和consumer服务都需要amqp依赖，因此这里把依赖直接放到父工程mq-demo中：

~~~xml
        <!--AMQP依赖，包含RabbitMQ-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-amqp</artifactId>
        </dependency>
~~~

步骤2：在publisher中编写测试方法，向simple.queue发送消息

1.在publisher服务中编写application.yml，添加mq连接信息：

```yml
logging:
  pattern:
    dateformat: MM-dd HH:mm:ss:SSS
spring:
  rabbitmq:
    host: 192.168.12.131 #主机名
    port: 5672 #端口
    username: mannor  #用户名
    password: 123321 #密码
    virtual-host: / #虚拟主机
```

2.在publisher服务中新建一个测试类，编写测试方法：

```java
package cn.itcast.mq.spring;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringAmqpTest {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    public void testSimpleQueue(){
        String queueName = "simple.queue";
        String message = "hello , spring amqp";
        rabbitTemplate.convertAndSend(queueName,message);
    }
}
```

> SpringAMQP如何发送消息？
>
> 1. 引入amqp的starter依赖。
>
> 2. 配置RabbitMQ地址。
>
> 3. 利用RabbitTemplate的convertAndSend方法。
>
>    [测试demo](https://gitee.com/rediaz/note-management-2/tree/master/SpringCloud/mq-demo - spring)

**接收：**

1. 依赖导入（发送demo导入过了）

   ```xml
       <!--AMQP依赖，包含RabbitMQ-->
       <dependency>
           <groupId>org.springframework.boot</groupId>
           <artifactId>spring-boot-starter-amqp</artifactId>
       </dependency>
   ```

   2. 配置文件：

      ```yml
      logging:
        pattern:
          dateformat: MM-dd HH:mm:ss:SSS
      spring:
        rabbitmq:
          host: 192.168.12.131 #主机名
          port: 5672 #端口
          username: mannor  #用户名
          password: 123321 #密码
          virtual-host: / #虚拟主机
      ```

   3. 编写监听类：

      ~~~java
      package cn.itcast.mq.listener;
      
      import org.springframework.amqp.rabbit.annotation.RabbitListener;
      import org.springframework.stereotype.Component;
      
      @Component
      public class SpringRabbitListener {
          
          @RabbitListener(queues = "simple.queue")
          public void listenSimpleQueue(String msg) {
              System.out.println("消费者接受到的simple.queue的消息为:【" + msg + "】");
          }
      }
      ~~~

### 3.3、Work Queue工作队列（注解声明队列）

- **Work queue**，**工作队列**，可以提高消息处理速度，避免队列消息堆积

  ![image-20240306153719491](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240306153719491.png)

案例实现：

模拟WorkQueue，实现一个队列绑定多个消费者

基本思路如下：

1. 在publisher服务中定义测试方法，每秒产生50条消息，发送到simple.queue

   ~~~java
   @RunWith(SpringRunner.class)
   @SpringBootTest
   public class SpringAmqpTest {
       @Autowired
       private RabbitTemplate rabbitTemplate;
   
       @Test
       public void testSimpleQueue() throws InterruptedException {
           String queueName = "simple.queue";
           String message = "hello , spring__";
           for (int i = 0; i < 50; i++) { //模拟每秒接受50条消息
               rabbitTemplate.convertAndSend(queueName,message+i);
               Thread.sleep(20);
           }
       }
   }
   ~~~

2. 在consumer服务中定义两个消息监听者，都监听simple.queue队列

   ~~~java
   @Component
   public class SpringRabbitListener {
   
       @RabbitListener(queues = "simple.queue")
       public void listenWorkQueue1(String msg) throws InterruptedException {
           System.out.println("consumer1 reserved simple.queue's message is :{" + msg + "}" + LocalTime.now());
           Thread.sleep(20);
       }
   
       @RabbitListener(queues = "simple.queue")
       public void listenWorkQueue2(String msg) throws InterruptedException {
           System.err.println("consumer2 ...... reserved simple.queue's message is :{" + msg + "}" + LocalTime.now());
           Thread.sleep(200);
       }
   }
   ~~~

3. 消费者1每秒处理50条消息，消费者2每秒处理10条消息（**预期结果**）

测试结果：

~~~java
consumer1 reserved simple.queue's message is :{hello , spring__0}15:47:54.547147400
consumer2 ...... reserved simple.queue's message is :{hello , spring__1}15:47:54.563943200
consumer1 reserved simple.queue's message is :{hello , spring__2}15:47:54.583777600
consumer1 reserved simple.queue's message is :{hello , spring__4}15:47:54.624799900
consumer1 reserved simple.queue's message is :{hello , spring__6}15:47:54.665670800
consumer1 reserved simple.queue's message is :{hello , spring__8}15:47:54.707454400
consumer1 reserved simple.queue's message is :{hello , spring__10}15:47:54.748139200
consumer2 ...... reserved simple.queue's message is :{hello , spring__3}15:47:54.765703700
consumer1 reserved simple.queue's message is :{hello , spring__12}15:47:54.791068600
consumer1 reserved simple.queue's message is :{hello , spring__14}15:47:54.833559700
consumer1 reserved simple.queue's message is :{hello , spring__16}15:47:54.875240300
consumer1 reserved simple.queue's message is :{hello , spring__18}15:47:54.916448300
consumer1 reserved simple.queue's message is :{hello , spring__20}15:47:54.957147500
consumer2 ...... reserved simple.queue's message is :{hello , spring__5}15:47:54.966136800
consumer1 reserved simple.queue's message is :{hello , spring__22}15:47:54.998661300
consumer1 reserved simple.queue's message is :{hello , spring__24}15:47:55.040466600
consumer1 reserved simple.queue's message is :{hello , spring__26}15:47:55.082932100
consumer1 reserved simple.queue's message is :{hello , spring__28}15:47:55.124183
consumer1 reserved simple.queue's message is :{hello , spring__30}15:47:55.166082900
consumer2 ...... reserved simple.queue's message is :{hello , spring__7}15:47:55.166587700
consumer1 reserved simple.queue's message is :{hello , spring__32}15:47:55.209400800
consumer1 reserved simple.queue's message is :{hello , spring__34}15:47:55.252836400
consumer1 reserved simple.queue's message is :{hello , spring__36}15:47:55.295680300
consumer1 reserved simple.queue's message is :{hello , spring__38}15:47:55.337493100
consumer2 ...... reserved simple.queue's message is :{hello , spring__9}15:47:55.367486100
consumer1 reserved simple.queue's message is :{hello , spring__40}15:47:55.378067700
consumer1 reserved simple.queue's message is :{hello , spring__42}15:47:55.419311400
consumer1 reserved simple.queue's message is :{hello , spring__44}15:47:55.460464100
consumer1 reserved simple.queue's message is :{hello , spring__46}15:47:55.503671500
consumer1 reserved simple.queue's message is :{hello , spring__48}15:47:55.546108800
consumer2 ...... reserved simple.queue's message is :{hello , spring__11}15:47:55.568499600
consumer2 ...... reserved simple.queue's message is :{hello , spring__13}15:47:55.770209700
consumer2 ...... reserved simple.queue's message is :{hello , spring__15}15:47:55.971745800
consumer2 ...... reserved simple.queue's message is :{hello , spring__17}15:47:56.172948
consumer2 ...... reserved simple.queue's message is :{hello , spring__19}15:47:56.373905800
consumer2 ...... reserved simple.queue's message is :{hello , spring__21}15:47:56.574553500
consumer2 ...... reserved simple.queue's message is :{hello , spring__23}15:47:56.777450500
consumer2 ...... reserved simple.queue's message is :{hello , spring__25}15:47:56.978813400
consumer2 ...... reserved simple.queue's message is :{hello , spring__27}15:47:57.180563800
consumer2 ...... reserved simple.queue's message is :{hello , spring__29}15:47:57.381982500
consumer2 ...... reserved simple.queue's message is :{hello , spring__31}15:47:57.583055500
consumer2 ...... reserved simple.queue's message is :{hello , spring__33}15:47:57.784108400
consumer2 ...... reserved simple.queue's message is :{hello , spring__35}15:47:57.985132900
consumer2 ...... reserved simple.queue's message is :{hello , spring__37}15:47:58.186041300
consumer2 ...... reserved simple.queue's message is :{hello , spring__39}15:47:58.386980
consumer2 ...... reserved simple.queue's message is :{hello , spring__41}15:47:58.587764400
consumer2 ...... reserved simple.queue's message is :{hello , spring__43}15:47:58.788255700
consumer2 ...... reserved simple.queue's message is :{hello , spring__45}15:47:58.989382300
consumer2 ...... reserved simple.queue's message is :{hello , spring__47}15:47:59.190647300
consumer2 ...... reserved simple.queue's message is :{hello , spring__49}15:47:59.392211400
~~~

> 最终发现测试结果与我们的预期 结果不相符合。
>
> 这是由于**RabbitMq**中的**消息预取**机制造成的：
>
> ![image-20240306155250130](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240306155250130.png)
>
> 解决方法：修改application.yml文件，设置**preFetch**这个值，可以控制**预取消息**的上限
>
> ~~~yml
> spring:
>   rabbitmq:
>     listener:
>       simple:
>         prefetch: 1 #每次只能获取一条消息，处理完成才能获取下一个消息
> ~~~

修改后结果：

![image-20240306155721649](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240306155721649.png)

### 3.4、发布（ Publish ）、订阅（ Subscribe ）

发布订阅模式与之前案例的区别就是允许将同一消息发送给多个消费者。实现方式是加入了**exchange**（交换机）。

![image-20240306160842226](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240306160842226.png)

常见exchange类型包括：

+ **Fanout**：广播

+ **Direct**：路由

+ **Topic**：话题

**注意**：exchange负责消息路由，而不是存储，路由失败则消息丢失

### 3.5、发布订阅-Fanout Exchange（配置Bean声明队列）

**Fanout Exchange** 会将接收到的消息广播到每一个跟其绑定的queue

![image-20240306161138525](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240306161138525.png)

**利用SpringAMQP演示FanoutExchange的使用**

实现思路如下：

1. 在consumer服务声明Exchange、Queue、Binding

   SpringAMQP提供了声明交换机、队列、绑定关系的API：

   ![image-20240306162840859](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240306162840859.png)

   在consumer服务上建一个类，添加@Configuration注解，并声明FanoutExchange、Queue和绑定关系对象Binding，代码如下：
   
   ```java
   package cn.itcast.mq.config;
   
   import org.springframework.amqp.core.Binding;
   import org.springframework.amqp.core.BindingBuilder;
   import org.springframework.amqp.core.FanoutExchange;
   import org.springframework.amqp.core.Queue;
   import org.springframework.context.annotation.Bean;
   import org.springframework.context.annotation.Configuration;
   
   @Configuration
   public class FanoutConfig {
       //    mannor.fanout 交换机
       @Bean
       public FanoutExchange fanoutExchange() {
           return new FanoutExchange("mannor.fanout");
       }
   
       //    fanout.queue1 队列1
       @Bean
       public Queue fanoutQueue1() {
           return new Queue("fanout.queue1");
       }
   
       // 绑定队列1到交换机
       @Bean
       public Binding fanoutBinding1(FanoutExchange fanoutExchange, Queue fanoutQueue1) {
           return BindingBuilder
                   .bind(fanoutQueue1)
                   .to(fanoutExchange);
       }
   
       //    fanout.queue2 队列2
       @Bean
       public Queue fanoutQueue2() {
           return new Queue("fanout.queue2");
       }
   
       // 绑定队列2到交换机
       @Bean
       public Binding fanoutBinding2(FanoutExchange fanoutExchange, Queue fanoutQueue2) {
           return BindingBuilder.bind(fanoutQueue2).to(fanoutExchange);
       }
   }
   ```

​		启动demo项目，就可以在RabbitMQ的控制台中显示新建的交换机和队列的信息。

2. 在consumer服务中，编写两个消费者方法，分别监听fanout.queue1和fanout.queue2

   ```java
   package cn.itcast.mq.listener;
   
   import org.springframework.amqp.rabbit.annotation.RabbitListener;
   import org.springframework.stereotype.Component;
   
   import java.time.LocalTime;
   
   @Component
   public class SpringRabbitListener {
   
       @RabbitListener(queues = "fanout.queue1")
       public void listenFanoutQueue1(String msg) {
           System.out.println("consumer reserved simple.queue's message is :【" + msg + "】");
       }
       @RabbitListener(queues = "fanout.queue2")
       public void listenFanoutQueue2(String msg) {
           System.out.println("consumer reserved simple.queue's message is :【" + msg + "】");
       }
   }
   ```

3. 在publisher中编写测试方法，向mannor.fanout发送消息

   ```java
   @RunWith(SpringRunner.class)
   @SpringBootTest
   public class SpringAmqpTest {
       @Autowired
       private RabbitTemplate rabbitTemplate;
   
       @Test
       public void testSendFanoutExchange() {
           //交换机名称
           String exchangeName = "mannor.fanout";
           //消息
           String message = "hello,every queue!";
           //发送
           rabbitTemplate.convertAndSend(exchangeName, "", message);
       }
   }
   ```

控制台输出：![image-20240306173804792](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240306173804792.png)

### 3.6、发布订阅-DirectExchange

**Direct Exchange** 会将接收到的消息根据规则**路由**到指定的**Queue**，因此称为**路由模式（routes）**。

+ 每一个**Queue**都与**Exchange**设置一个**BindingKey**

+ **发布者**发送消息时，指定消息的**RoutingKey**

+ **Exchange**将消息路由到**BindingKey**与消息**RoutingKey**一致的队列

**利用SpringAMQP演示DirectExchange的使用**：

实现思路如下：

1. 利用@RabbitListener声明Exchange、Queue、RoutingKey

2. 在consumer服务中，编写两个消费者方法，分别监听direct.queue1和direct.queue2

   ~~~java
   @Component
   public class SpringRabbitListener {
   
       @RabbitListener(bindings = @QueueBinding(
               value = @Queue(name = "direct.queue1"),
               exchange = @Exchange(name = "mannor.direct", type = ExchangeTypes.DIRECT),
               key = {"red", "blue"}
       ))
       public void listenDirectQueue1(String msg) {
           System.out.println("consumer reserved direct.queue's message is :【" + msg + "】");
       }
   
       @RabbitListener(bindings = @QueueBinding(
               value = @Queue(name = "direct.queue2"),
               exchange = @Exchange(name = "mannor.direct", type = ExchangeTypes.DIRECT),
               key = {"red", "yellow"}
       ))
       public void listenDirectQueue2(String msg) {
           System.out.println("consumer reserved direct.queue's message is :【" + msg + "】");
       }
   }
   ~~~

3. 在publisher中编写测试方法，向itcast. direct发送消息

   ```java
   @RunWith(SpringRunner.class)
   @SpringBootTest
   public class SpringAmqpTest {
       @Autowired
       private RabbitTemplate rabbitTemplate;
       
       @Test
       public void testSendDirectExchange() {
           //交换机名称
           String exchangeName = "mannor.direct";
           //消息
           String message = "hello,every queue!";
           //发送
           rabbitTemplate.convertAndSend(exchangeName, "blue", message);
       }
   
   }
   ```

### 3.7、发布订阅-TopicExchange

TopicExchange与DirectExchange类似，区别在于routingKey必须是**多个单词**的列表，并且以 `.` 分割。

Queue与Exchange指定BindingKey时可以使用通配符：

**#：代指0个或多个单词**

**\*：代指一个单词**

> 例如：
>
> china.news 代表有中国的新闻消息；
>
> china.weather 代表中国的天气消息；
>
> japan.news 则代表日本新闻
>
> japan.weather 代表日本的天气消息；

**利用SpringAMQP演示TopicExchange的使用**

实现思路如下：

1. 并利用@RabbitListener声明Exchange、Queue、RoutingKey

2. 在consumer服务中，编写两个消费者方法，分别监听topic.queue1和topic.queue2

   ~~~java
   @Component
   public class SpringRabbitListener {
      
       @RabbitListener(bindings = @QueueBinding(
               value = @Queue(name = "topic.queue1"),
               exchange = @Exchange(name = "mannor.topic", type = ExchangeTypes.TOPIC),
               key = {"china.#"} //收到来自china的所有信息
       ))
       public void listenTopicQueue1(String msg) {
           System.out.println("消费者收到来自 topic.queue 的信息 :【" + msg + "】");
       }
   
       @RabbitListener(bindings = @QueueBinding(
               value = @Queue(name = "topic.queue2"),
               exchange = @Exchange(name = "mannor.topic", type = ExchangeTypes.TOPIC),
               key = {"#.news"}
       ))
       public void listenTopicQueue2(String msg) {
           System.out.println("消费者收到来自 topic.queue 的信息 :【" + msg + "】");
       }
   }
   ~~~

3. 在publisher中编写测试方法，向amnnor. topic发送消息

   ```java
   @Test
   public void testSendTopicExchange() {
       //交换机名称
       String exchangeName = "mannor.topic";
       //消息
       String message = "曼诺尔雷迪亚兹入驻抖音啦！";
       //发送
       rabbitTemplate.convertAndSend(exchangeName, "china.news", message);
   }
   ```

   > 具体看的是前缀和后缀，主要 **`#`：代指0个或多个单词  ；   `*`：代指一个单词**。

### 3.8、SpringAMQP-消息转换器

**测试发送Object类型消息**

正如我们之前所学习的那样，其实SpringAMQP通过队列传输的数据都是Object类型的，如下图所示：

![image-20240306195648984](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240306195648984.png)

所以我们现在就来尝试一下对象的传输。

1. 首先创建一个queue的消息队列：（创还能队列有两种方式，之前案例中两种都有涉及到，下面这种是通过配置文件创建）

   ```java
   @Configuration
   public class FanoutConfig {
       @Bean
       public Queue objectQueue(){
               return new Queue("object.queue");
       }
   }
   ```

2. 在发送者这里编写测试类：

   ```java
   @RunWith(SpringRunner.class)
   @SpringBootTest
   public class SpringAmqpTest {
       @Autowired
       private RabbitTemplate rabbitTemplate;
       @Test
       public void testSendObjectQueue() {
           Map<String, Object> obj = new HashMap<>();
           obj.put("姓名", "曼诺尔雷迪亚兹");
           obj.put("性别", "男");
           obj.put("年龄", "45");
           rabbitTemplate.convertAndSend("object.queue", obj);
       }
   }
   ```

3. 查看RabbitMQ控制台中发送的消息如下图所示，我们会发现原生api只支持字节的形式，而SpringAMQP支持我们发送Object对象，说明传输方式会将我们的对象进行java序列化来传输。

   这种传输会出现一些问题，就是性能较差；安全性不高，容易注入；数据长度太长，不利于传输消息。

   ![image-20240306201930243](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240306201930243.png)



所以这里需要使用到**消息转换器**的知识。

Spring的对消息对象的处理是由**org.springframework.amqp.support.converter.MessageConverter**来处理的。而默认实现是

**SimpleMessageConverter**，基于**JDK**的**ObjectOutputStream**完成序列化。

如果要修改只需要定义一个MessageConverter 类型的Bean即可解决上面的问题。推荐用JSON方式序列化，步骤如下：

1. 我们在publisher服务引入依赖

   ```xml
   <!--   json 序列化工具     -->
   <dependency>
       <groupId>com.fasterxml.jackson.core</groupId>
       <artifactId>jackson-databind</artifactId>
   </dependency>
   ```

2. 声明Bean（在启动类或者配置类上声明）

   ```java
   @SpringBootApplication
   public class PublisherApplication {
       
       public static void main(String[] args) {
           SpringApplication.run(PublisherApplication.class);
       }
   
       @Bean
       public MessageConverter messageConverter() {
           return new Jackson2JsonMessageConverter();
       }
   }
   ```

3. 由于消息转换是底层实现的，所以我们无序额外的操作，查看RabbitMQ的消息情况，发现现在传输的就是JSON对象。

   ![image-20240306203416982](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240306203416982.png)

   

   **测试发送Object类型消息**

1. 在接收者中引入Jackson依赖

   ~~~xml
   <!--   json 序列化工具     -->
   <dependency>
       <groupId>com.fasterxml.jackson.core</groupId>
       <artifactId>jackson-databind</artifactId>
   </dependency>
   ~~~

2. 配置转换器的Bean对象

   ~~~java
   @SpringBootApplication
   public class ConsumerApplication {
       public static void main(String[] args) {
           SpringApplication.run(ConsumerApplication.class, args);
       }
       @Bean
       public MessageConverter messageConverter() {
           return new Jackson2JsonMessageConverter();
       }
   }
   ~~~

3. 定义一个消费者监听类，监听队列中的消息

   ```java
   @Component
   public class SpringRabbitListener {
       
       @RabbitListener(queues = "object.queue")
       public void listenObjectQueue(Map<String,Object> obj){
           System.out.println("消费者收到来自发送者发送的对象：" + obj );
       }
   }
   ```

4. 运行结果

   ![image-20240306204715049](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240306204715049.png)

> SpringAMQP中消息的序列化和反序列化是怎么实现的？
>
> - 利用MessageConverter实现的，默认是JDK的序列化
>
> + 注意发送方与接收方必须使用相同的MessageConverter

测试项目源码：[RabbitMq demo源码](https://gitee.com/rediaz/note-management-2/tree/master/SpringCloud/mq-demo%20-%20spring)

