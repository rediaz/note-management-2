# Feign的介绍，为什么要用Feign？

[TOC]

## 1、RestTemplate方式调用存在的问题

先来利用RestTemplate发起远程调用的代码:

```java
//2. 利用RestTemplate发起HTTP请求，查询user
String url = "http://userservice/user/" + order.getUserId();
User user = restTemplate.getForObject(url, User.class);
```

存在下面的问题：

- 代码可读性差，编程体验不统一

- 参数复杂URL难以维护

> 假如参数众多，则会：
>
> ![image-20240228155202315](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240228155202315.png)

所以我们需要用到另外一个工具--**Feign**

## 2、Feign的介绍

Feign是一个声明式的http客户端，官方地址：[https://github.com/OpenFeign/feign](https://github.com/OpenFeign/feign)

其作用就是帮助我们优雅的实现http请求的发送，解决上面提到的问题

![image-20240228155546836](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240228155546836.png)

## 3、Feign快速入门

1. 引入依赖

```xml
<!--   feign的依赖     -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-openfeign</artifactId>
</dependency>
```

2. 在order-service的启动类添加注解开启Feign的功能：

```java
@EnableFeignClients
@MapperScan("cn.itcast.order.mapper")
@SpringBootApplication
public class OrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class, args);
    }
}
```

3. 编写Feign客户端：

```java
package cn.itcast.order.clients;

@FeignClient("userservice")
public interface UserClient {
    @GetMapping("/user/{id}")
    User findById(@PathVariable("id") Long id);
}
```

> 主要是基于SpringMVC的注解来声明远程调用的信息，比如：
>
> * 服务名称：userservice
>
> * 请求方式：GET
>
> * 请求路径：/user/{id}
>
> * 请求参数：Long id
>
> * 返回值类型：User

4. 用Feign客户端代替RestTemplate

```java
@Service
public class OrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private UserClient userClient;

    public Order queryOrderById(Long orderId) {
        // 1.查询订单
        Order order = orderMapper.findById(orderId);
        //2. 利用Feign发起http请求，查询user
        User user = userClient.findById(order.getUserId());
        // 3.封装user到Order
        order.setUser(user);
        // 4.返回
        return order;
    }
}
```

5. 启动nacos并测试接口，发现功能测试没有问题，并且已经自动实现了负载均衡，原因是强大的Feign里面已经集成了ribbon，自动实现了负载均衡的功能

![image-20240228162432159](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240228162432159.png)

## 4、自定义Feign的配置

Feign运行自定义配置来覆盖默认配置，可以修改的配置如下：

| 类型                   | 作用             | 说明                                                   |
| ---------------------- | ---------------- | ------------------------------------------------------ |
| **feign.Logger.Level** | 修改日志级别     | 包含四种不同的级别：NONE、BASIC、HEADERS、FULL         |
| feign.codec.Decoder    | 响应结果的解析器 | http远程调用的结果做解析，例如解析json字符串为java对象 |
| feign.codec.Encoder    | 请求参数编码     | 将请求参数编码，便于通过http请求发送                   |
| feign. Contract        | 支持的注解格式   | 默认是SpringMVC的注解                                  |
| feign. Retryer         | 失败重试机制     | 请求失败的重试机制，默认是没有，不过会使用Ribbon的重试 |

我们一般会配置日志级别

### 修改Feign日志

- 方式一：配置文件方式

  1. 全局生效

     ```yml
     feign:
       client:
         config:
           default: #这里用default就是全局配置，如果是写服务名称，则是针对某个微服务的配置
             loggerLevel: FULL #日志级别
     ```

  2. 日志级别

     ~~~yml
     feign:
       client:
         config:
           userservice: #这里用default就是全局配置，如果是写服务名称，则是针对某个微服务的配置
             loggerLevel: FULL #日志级别
     ~~~

- 方式二：java代码方式，需要先声明一个Bean：

  ```java
  package cn.itcast.order.config;
  
  import feign.Logger;
  import org.springframework.context.annotation.Bean;
  
  public class FeignClientConfiguration {
      @Bean
      public Logger.Level logLeval(){
          return Logger.Level.BASIC;
      }
  }
  ```

- 使配置生效：

  - 如果是全局配置，则把它放到@EnableFeignClients这个注解中：

  - ```java
    @EnableFeignClients(defaultConfiguration  = FeignClientConfiguration.class) //全局配置
    @MapperScan("cn.itcast.order.mapper")
    @SpringBootApplication
    public class OrderApplication {
    
        public static void main(String[] args) {
            SpringApplication.run(OrderApplication.class, args);
        }
    }
    ```

  - 如果是局部配置，则把它放到@FeignClient这个注解中：

    ```java
    @FeignClient(value = "userservice", configuration = FeignClientConfiguration.class) //局部，只在userservice中生效
    public interface UserClient {
        @GetMapping("/user/{id}")
        User findById(@PathVariable("id") Long id);
    }
    ```