package cn.pzhu.gateway;

import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * 权限认证过滤器（全局过滤器的实现）
 * <p>
 * 需求：定义全局过滤器，拦截请求，判断请求的参数是否满足下面条件：
 * 参数中是否有authorization，
 * authorization参数值是否为admin
 * 如果同时满足则放行，否则拦截
 */
@Order(-1) //定义优先级，优先级越高，里面的参数值越低，可以看源码学习（也可以实现Ordered接口实现不再赘述）
@Component //将AuthorizeFilter注入到spring作为一个bean
public class AuthorizeFilter implements GlobalFilter {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //1.获取请求参数
        ServerHttpRequest request = exchange.getRequest();
        MultiValueMap<String, String> params = request.getQueryParams();
        //2.获取参数中的authorization参数
        String auth = params.getFirst("authorization"); //设置需要过滤的参数
        //3.判断参数值是否等于admin
        if ("admin".equals(auth)) {
            //4.是，放行
            return chain.filter(exchange);
        }
        //5. 否，拦截 (为了让用户体验到返回的结果，这里就设置响应码401，即未登录状态图码)
        //5.1 设置状态码
        exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED); //UNAUTHORIZED是401状态码的枚举类型
        //5.2 拦截请求
        return exchange.getResponse().setComplete(); //设置指示完成或错误
    }
}
