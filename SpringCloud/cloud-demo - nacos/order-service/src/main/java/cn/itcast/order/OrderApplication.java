package cn.itcast.order;


import cn.pzhu.feign.clients.UserClient;
import cn.pzhu.feign.config.FeignClientConfiguration;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@EnableFeignClients(clients= UserClient.class,defaultConfiguration  = FeignClientConfiguration.class) //全局
@MapperScan("cn.itcast.order.mapper")
@SpringBootApplication
public class OrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class, args);
    }

    /**
     * 创建RestTemplate并且注入Spring容器
     *
     * @return
     */
    @Bean
    @LoadBalanced //负载均衡
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }


    /**
     * 通过这个设置就将负载均衡的规则设置为随机
     *
     * @return
     */
  /*  @Bean
    public IRule randomRule() {
        return new RandomRule();
    }*/
}