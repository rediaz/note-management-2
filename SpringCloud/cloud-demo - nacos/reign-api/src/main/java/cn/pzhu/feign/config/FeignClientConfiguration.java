package cn.pzhu.feign.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;

public class FeignClientConfiguration {
    @Bean
    public Logger.Level logLeval(){
        return Logger.Level.BASIC;
    }
}
