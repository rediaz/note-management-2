package cn.itcast.user.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "pattern")
public class ParttenPropertis {
    private String dataformat; //上面注解上的前缀与dataformat进行匹配，就可以实现配置的自动更新
    private String envShardValue;

}
