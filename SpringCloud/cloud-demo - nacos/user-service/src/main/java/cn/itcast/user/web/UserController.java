package cn.itcast.user.web;

import cn.itcast.user.config.ParttenPropertis;
import cn.itcast.user.pojo.User;
import cn.itcast.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Slf4j
@RestController
@RequestMapping("/user")
//@RefreshScope //实现Nacos配置热更新的方式之一
public class UserController {

    @Autowired
    private UserService userService;

//    @Value("${pattern.dataformat}") //测试是否可以读取Nacos设置的配置文件的配置信息
//    private String dataformat;

    @Autowired
    private ParttenPropertis propertis;


    @GetMapping("now")
    public String now() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern(propertis.getDataformat()));
    }

    @GetMapping("prop")
    public ParttenPropertis propertis() {
        return propertis;
    }

    /**
     * 路径： /user/110
     *
     * @param id 用户id
     * @return 用户
     */
    @GetMapping("/{id}")
    public User queryById(@PathVariable("id") Long id, @RequestHeader(value = "hello", required = false) String hello) {
        System.out.println("hello:" + hello);
        return userService.queryById(id);
    }
}
