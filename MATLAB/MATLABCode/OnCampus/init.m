% clc;clear all;
%%单个文件的导入
% excel
final = xlsread('2021MCMProblemC_DataSet.xlsx') ;
% for i=1:5 
%     final (:, 2*i-1:2*i)=xlsread('2021MCMProblemC_DataSet.xlsx',['sheet',num2str(i)]);
% end
A = table2array(table1);

%%多个数据
len = height(A); % 获取table A 的行数

GlobalID = cell(1, len);
Longitude = cell(1, len);

for i = 1:len
    file_path = A.GlobalID{i}; % 获取每一行的GlobalID值作为文件路径
    data = importdata(file_path); % 使用GlobalID作为文件路径导入数据
    % 在这里对导入的数据进行处理
end

