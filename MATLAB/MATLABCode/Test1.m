t = 0: pi/50:10*pi ;
plot3(sin(t),cos(t),t)  %plot3三维绘图的标志，参数分别是x，y，z
xlabel('sin(t)') %x轴的标签
ylabel('cos(t)')
zlabel('t')
%hold on  %中使用“保持”命令来保留当前绘图，同时向同一图形添加新的绘图。这允许在同一图形上显示多个绘图。
%hold off  %“暂停”命令用于关闭暂停功能，这意味着后续绘图将替换图中的现有绘图，而不是添加到图中。
grid on  %添加网格线
axis square %形成正方形图