%sparse生成稀疏矩阵，也就是除了注明的几个元素外，其余都是0
%spare里第一个和第二个矩阵相同位置的元素值就是非零元素的索引
%非零元素的值
%w是每条边的权值
w = [10,5,2,1,4,6,7,3,9,2]
DG = sparse([1,1,2,2,3,4,4,5,5,5],[2,5,5,3,4,3,1,2,3,4],w)
% 没有就默认为零，这样快速生成一个稀疏矩阵