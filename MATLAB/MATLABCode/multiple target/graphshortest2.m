% dist是最短路径的值, path是最短路径的节点顺序
% pred是到每一个节点的最短路径的终点前一个节点
% 如果求节点1到其他所有节点的最短路径呢?
[dist,path,pred] = graphshortestpath(DG,1,3)  %后面的数字参数是起点和终点，然后计算最短路径