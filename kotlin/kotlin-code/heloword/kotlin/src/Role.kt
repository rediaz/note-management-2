fun main() {

}

fun handerHealth(role: Weapon) {
    when (role) {
        is PlayerType1 -> println("玩家1")
        is PlayerType2 -> println("玩家2")
        is PlayerType3 -> println("玩家3")
        is EnemyType1 -> println("敌人1")
        is EnemyType2 -> println("敌人2")
        is EnemyType3 -> println("敌人3")
    }
}
fun handerWeapon(role: Weapon) {
    when (role) {
        is PlayerType1 -> println("玩家1")
        is PlayerType2 -> println("玩家2")
        is PlayerType3 -> println("玩家3")
        is EnemyType1 -> println("敌人1")
        is EnemyType2 -> println("敌人2")
        is EnemyType3 -> println("敌人3")
    }
}


sealed interface Health {}
sealed interface Weapon {}
class PlayerType1 : Health, Weapon
class PlayerType2 : Health, Weapon
class PlayerType3 : Health, Weapon
class EnemyType1 : Health, Weapon
class EnemyType2 : Health, Weapon
class EnemyType3 : Health, Weapon