
/**
 * 主函数，程序的入口点。
 * 实现了用户登录和退出登录的处理。
 */
fun main() {
    // 处理用户登录请求
    handerMainIntent(MainIntent.Login("李明", "123456"))
    // 处理用户退出登录请求
    handerMainIntent(MainIntent.Logout)
}

/**
 * 主意图 sealed 类，用于封装所有主功能的操作。
 */
sealed class MainIntent {
    /**
     * 用户登录意图数据类。
     * @param username 用户名。
     * @param password 密码。
     */
    data class Login(val username: String, val password: String) : MainIntent()

    /**
     * 用户退出登录意图。
     */
    object Logout : MainIntent()
}

/**
 * 处理主意图的函数。
 * 根据传入的 MainIntent 对象执行相应的操作。
 * @param mainIntent 用户的主意图，可以是登录或退出登录。
 */
fun handerMainIntent(mainIntent: MainIntent) {
    when (mainIntent) {
        is MainIntent.Login -> userLoginRequest(mainIntent.username, mainIntent.password) // 处理用户登录请求
        MainIntent.Logout -> println("退出登录.....") // 处理用户退出登录请求
    }
}

/**
 * 处理用户登录请求的函数。
 * @param username 用户名。
 * @param password 密码。
 * 打印用户登录信息。
 */
fun userLoginRequest(username: String, password: String) {
    println("用户登录：${username} ${password}")
}