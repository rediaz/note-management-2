package com.example.accessingdatamysql.controller

import com.example.accessingdatamysql.pojo.R
import com.example.accessingdatamysql.pojo.User
import com.example.accessingdatamysql.Service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.query.Param
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/user")
class UserController {


    @Autowired
    lateinit var userService: UserService


    @GetMapping()
    fun getUserByUsername(@Param("username") username: String): R<User> {
        return userService.getUserByUsername(username)
    }

    @PostMapping
    fun insertUser(@RequestParam username: String, @RequestParam password: String): R<String> {
        return userService.InsertUser(User(username, password))
    }

}