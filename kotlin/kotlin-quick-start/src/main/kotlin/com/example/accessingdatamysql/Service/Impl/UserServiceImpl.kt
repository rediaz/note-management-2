package com.example.accessingdatamysql.Service.Impl

import com.example.accessingdatamysql.mapper.UserMapper
import com.example.accessingdatamysql.pojo.R
import com.example.accessingdatamysql.pojo.User
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl
import com.example.accessingdatamysql.Service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserServiceImpl : UserService, ServiceImpl<UserMapper, User>() {

    @Autowired
    lateinit var userMapper: UserMapper

    override fun getUserByUsername(userName: String): R<User> {
        var queryWrapper = QueryWrapper<User>()
        queryWrapper.eq("username", userName)
        var user = userMapper.selectOne(queryWrapper)
        return R.success(user)
    }

    override fun InsertUser(user: User): R<String> {
        var queryWrapper = QueryWrapper<User>()
        queryWrapper.eq("username", user.username)
        var userList = userMapper.selectList(queryWrapper)
        println("user:$userList userLength:${userList.size}")
        if (userList.size > 0) return R.error("username 已经存在")
        if (userMapper.insert(user) > 0) {
            return R.success("insert success !")
        } else {
            return R.error("insert fail")
        }

    }
}

