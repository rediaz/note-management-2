package com.example.accessingdatamysql.pojo

import java.io.Serializable


data class User(val username: String, val password: String): Serializable {}