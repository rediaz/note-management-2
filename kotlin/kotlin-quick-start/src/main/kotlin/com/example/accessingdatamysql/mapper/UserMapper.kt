package com.example.accessingdatamysql.mapper

import com.example.accessingdatamysql.pojo.User
import com.baomidou.mybatisplus.core.mapper.BaseMapper
import org.apache.ibatis.annotations.Mapper


@Mapper
interface UserMapper : BaseMapper<User> {}