package com.example.accessingdatamysql.Service

import com.example.accessingdatamysql.pojo.R
import com.example.accessingdatamysql.pojo.User
import com.baomidou.mybatisplus.extension.service.IService

open interface UserService : IService<User> {
    fun getUserByUsername(userName: String): R<User>
    fun InsertUser(user: User): R<String>
}