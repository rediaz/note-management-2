package com.example.accessingdatamysql.pojo

import java.io.Serializable

import java.util.HashMap

/**
 * 通用返回的结果类，服务器端返回的数据终将都会封装成此对象
 * @param <T>
 */
data class R<T>(
    var code: Int = 0, // 编码：1成功，0和其它数字为失败
    var msg: String? = null, // 错误信息
    var data: T? = null, // 数据
    val map: MutableMap<String, Any> = HashMap() // 动态数据
) : Serializable {

    companion object {

        inline fun <reified T> success(data: T): R<T> {
            return R(code = 1, data = data)
        }

        fun <T> error(msg: String): R<T> {
            return R(code = 0, msg = msg)
        }
    }

    fun add(key: String, value: Any): R<T> {
        map[key] = value
        return this
    }
}