## AI组件使用-通义灵码

通义灵码的本地插件安装包下载后

在你的IDE（在此文档中以DevEco Studio为例）中点击file -> settings ->plugins

![image-20240116153542063](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240116153542063.png)

找到设置按钮，选择从本地磁盘安装插件

![image-20240116153621191](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240116153621191.png)

按照提示安装完成重启IDE后右边可以看到通义灵码，登录一下(使用支付宝或者钉钉扫码都可以) 登录后即可使用

