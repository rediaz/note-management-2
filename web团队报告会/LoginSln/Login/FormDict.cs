﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace Login
{
    public partial class FormDict : Form
    {
        Dictionary<string, Student> dictStus = null;
        Stus ss = new Stus();
        public FormDict()
        {
            InitializeComponent();
        }

        private void btnLstInit_Click(object sender, EventArgs e)
        {
            dictStus = new Dictionary<string, Student>();
            ss = new Stus();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {            
            Student s = new Student("01", "张飞", DateTime.Parse("1980-1-1"));
            dictStus[s.Sno] = s;
            ss[s.Sno] = s;
            s = new Student("02", "关羽", DateTime.Parse("1981-8-1"));
            dictStus[s.Sno] = s;
            ss[s.Sno] = s;
            s = new Student("15", "王五", DateTime.Parse("1983-10-1"));
            dictStus[s.Sno] = s;
            ss[s.Sno] = s;
            s = new Student("03", "刘备", DateTime.Parse("1983-10-1"));
            dictStus[s.Sno] = s;
            ss[s.Sno] = s;
            s = new Student("05", "李四", DateTime.Parse("1983-10-1"));
            dictStus[s.Sno] = s;
            s = new Student("06", "王五", DateTime.Parse("1983-10-1"));
            dictStus[s.Sno] = s;
            ss[s.Sno] = s;
            s = new Student("09", "王五", DateTime.Parse("1983-10-1"));
            dictStus.Add(s.Sno, s);
            ss[s.Sno] = s;
            s = new Student("10", "王五", DateTime.Parse("1983-10-1"));
            dictStus[s.Sno] = s;
            ss[s.Sno] = s;
            s = new Student("14", "王五", DateTime.Parse("1983-10-1"));
            dictStus[s.Sno] = s;
            ss[s.Sno] = s;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textBox1.Text))
            {
                if (dictStus.ContainsKey(textBox1.Text))
                {
                    Text = dictStus[textBox1.Text].SayHello();
                }
            }
        }

        private void btnRecursive_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            foreach (string sno in dictStus.Keys)
            {
                Student s = dictStus[sno];
                listBox1.Items.Add(s.SayHello());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            foreach (Student s in dictStus.Values)
            {
                listBox1.Items.Add(s.SayHello());
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            foreach (KeyValuePair<string, Student> kv in dictStus)
            {
                listBox1.Items.Add(kv.Value.SayHello());
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textBox1.Text))
            {
                if (dictStus.ContainsKey(textBox1.Text))
                {
                    dictStus.Remove(textBox1.Text);
                }
            }
        }

        private void btnModified_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textBox1.Text))
            {
                if (dictStus.ContainsKey(textBox1.Text))
                {
                    dictStus[textBox1.Text] = new Student("05", "Test", DateTime.Parse("1985-10-1"));
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string name = textBox1.Text == "" ? "王五" : textBox1.Text;
            List<Student> lstStu = ss[name, 1];
            listBox1.Items.Clear();
            foreach (Student s in lstStu)
            {
                listBox1.Items.Add(s.SayHello());
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < ss.Count; i++)
            {
                listBox1.Items.Add(ss[i].SayHello());
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            foreach(int i in ss.Indexes)
            {
                listBox1.Items.Add(ss[i].SayHello());
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            foreach (string sno in ss.Snoes)
            {
                listBox1.Items.Add(ss[sno].SayHello());
            }
        }
    }

    public class Student
    {
        string sno;

        public Student(string sno, string name, DateTime birth)
        {
            this.sno = sno;
            Name = name;
            Birth = birth;
        }

        public string Sno
        {
            get
            {
                return sno;
            }
            set
            {
                sno = value;
            }
        }

        public string Name
        {
            get;
            set;
        }

        public DateTime Birth
        {
            get;
            set;
        }

        public string SayHello()
        {
            string fmt = "Hello, 我是{0}，学号：{1}，生日：{2}";
            return string.Format(fmt, Name, Sno, Birth.ToString());
        }
    }

    public class Stus
    {
        Dictionary<string, Student> dictStus = new Dictionary<string, Student>();

        public List<Student> this[string name, int Flag]
        {
            get
            {
                List<Student> lstStu = new List<Student>();
                foreach (Student s in dictStus.Values)
                {
                    if (s.Name == name)
                    {
                        lstStu.Add(s);
                    }
                }
                return lstStu;
            }
        }

        public Student this[string sno]
        {
            get
            {
                Student rtn = null;
                if (dictStus.ContainsKey(sno))
                {
                    rtn = dictStus[sno];
                }
                return rtn;
            }
            set
            {
                dictStus[sno] = value;
            }
        }

        public int Count
        {
            get
            {
                return dictStus.Count;
            }
        }

        public Student this[int index]
        {
            get
            {
                Student rtn = null;
                List<string> lstSnoes = dictStus.Keys.ToList();
                if (index < lstSnoes.Count)
                {
                    rtn = dictStus[lstSnoes[index]];
                }
                return rtn;
            }
        }

        public List<int> Indexes
        {
            get
            {
                List<int> lstIdxes = new List<int>();
                List<string> lstSnoes = dictStus.Keys.ToList();
                int i = 0;
                foreach(string sno in lstSnoes)
                {
                    lstIdxes.Add(i);
                    i++;
                }
                return lstIdxes;
            }
        }

        public List<string> Snoes
        {
            get
            {
                return dictStus.Keys.ToList();
            }
        }
    }
}
