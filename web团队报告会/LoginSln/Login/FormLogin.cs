﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Login
{
    public partial class FormLogin : Form
    {
        public FormLogin()
        {
            InitializeComponent();
        }

        public string UserName
        {
            get
            {
                return tbUser.Text.ToLower();
            }
        }

        public string Password
        {
            get
            {
                return tbPass.Text.ToLower();
            }
        }

        public string GetUserName()
        {
            return tbUser.Text.ToLower();
        }

        private void tbUser_KeyUp(object sender, KeyEventArgs e)
        {
            //NULL  ""  "    "
            if (!string.IsNullOrEmpty(tbUser.Text))
            {
                if (e.KeyCode == Keys.Enter)
                {
                    tbPass.Focus();
                }
            }
        }

        private void tbPass_KeyUp(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty(tbPass.Text))
            {
                if (e.KeyCode == Keys.Enter)
                {
                    btnLogin.PerformClick();
                }
            }
        }

        private void tbUser_TextChanged(object sender, EventArgs e)
        {
            btnLogin.Enabled = tbUser.Text != "" && tbPass.Text != "";
        }
        private void tbPass_TextChanged(object sender, EventArgs e)
        {
            btnLogin.Enabled = tbUser.Text != "" && tbPass.Text != "";
        }
    }
}
