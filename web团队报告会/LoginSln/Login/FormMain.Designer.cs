﻿namespace Login
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.msMain = new System.Windows.Forms.MenuStrip();
            this.miSysMgr = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiChangePass = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiLogOff = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiExit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiCalc = new System.Windows.Forms.ToolStripMenuItem();
            this.miStuMgr = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiStuBaseInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.miCourse = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCourse = new System.Windows.Forms.ToolStripMenuItem();
            this.miSC = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSC = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiLoop = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCollectOp = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDict = new System.Windows.Forms.ToolStripMenuItem();
            this.miWins = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiView = new System.Windows.Forms.ToolStripMenuItem();
            this.msMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // msMain
            // 
            this.msMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miSysMgr,
            this.miStuMgr,
            this.miCourse,
            this.miSC,
            this.miWins});
            this.msMain.Location = new System.Drawing.Point(0, 0);
            this.msMain.Name = "msMain";
            this.msMain.Size = new System.Drawing.Size(800, 25);
            this.msMain.TabIndex = 0;
            this.msMain.Text = "menuStrip1";
            // 
            // miSysMgr
            // 
            this.miSysMgr.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiChangePass,
            this.tsmiSeparator1,
            this.tsmiLogOff,
            this.tsmiExit,
            this.tsmiSeparator2,
            this.tsmiCalc});
            this.miSysMgr.Name = "miSysMgr";
            this.miSysMgr.Size = new System.Drawing.Size(68, 21);
            this.miSysMgr.Text = "系统管理";
            // 
            // tsmiChangePass
            // 
            this.tsmiChangePass.Name = "tsmiChangePass";
            this.tsmiChangePass.Size = new System.Drawing.Size(124, 22);
            this.tsmiChangePass.Text = "更改口令";
            this.tsmiChangePass.Click += new System.EventHandler(this.tsmiChangePass_Click);
            // 
            // tsmiSeparator1
            // 
            this.tsmiSeparator1.Name = "tsmiSeparator1";
            this.tsmiSeparator1.Size = new System.Drawing.Size(121, 6);
            // 
            // tsmiLogOff
            // 
            this.tsmiLogOff.Name = "tsmiLogOff";
            this.tsmiLogOff.Size = new System.Drawing.Size(124, 22);
            this.tsmiLogOff.Text = "注销";
            this.tsmiLogOff.Click += new System.EventHandler(this.tsmiLogOff_Click);
            // 
            // tsmiExit
            // 
            this.tsmiExit.Name = "tsmiExit";
            this.tsmiExit.Size = new System.Drawing.Size(124, 22);
            this.tsmiExit.Text = "退出";
            this.tsmiExit.Click += new System.EventHandler(this.tsmiExit_Click);
            // 
            // tsmiSeparator2
            // 
            this.tsmiSeparator2.Name = "tsmiSeparator2";
            this.tsmiSeparator2.Size = new System.Drawing.Size(121, 6);
            // 
            // tsmiCalc
            // 
            this.tsmiCalc.Name = "tsmiCalc";
            this.tsmiCalc.Size = new System.Drawing.Size(124, 22);
            this.tsmiCalc.Text = "计算器";
            this.tsmiCalc.Click += new System.EventHandler(this.tsmiCalc_Click);
            // 
            // miStuMgr
            // 
            this.miStuMgr.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiStuBaseInfo,
            this.tsmiView});
            this.miStuMgr.Name = "miStuMgr";
            this.miStuMgr.Size = new System.Drawing.Size(68, 21);
            this.miStuMgr.Text = "学生管理";
            // 
            // tsmiStuBaseInfo
            // 
            this.tsmiStuBaseInfo.Name = "tsmiStuBaseInfo";
            this.tsmiStuBaseInfo.Size = new System.Drawing.Size(180, 22);
            this.tsmiStuBaseInfo.Text = "学生基本信息管理";
            this.tsmiStuBaseInfo.Click += new System.EventHandler(this.tsmiStuBaseInfo_Click);
            // 
            // miCourse
            // 
            this.miCourse.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiCourse});
            this.miCourse.Name = "miCourse";
            this.miCourse.Size = new System.Drawing.Size(68, 21);
            this.miCourse.Text = "课程管理";
            // 
            // tsmiCourse
            // 
            this.tsmiCourse.Name = "tsmiCourse";
            this.tsmiCourse.Size = new System.Drawing.Size(148, 22);
            this.tsmiCourse.Text = "课程信息管理";
            this.tsmiCourse.Click += new System.EventHandler(this.tsmiCourse_Click);
            // 
            // miSC
            // 
            this.miSC.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiSC,
            this.tsmiLoop,
            this.tsmiCollectOp,
            this.tsmiDict});
            this.miSC.Name = "miSC";
            this.miSC.Size = new System.Drawing.Size(68, 21);
            this.miSC.Text = "选课管理";
            // 
            // tsmiSC
            // 
            this.tsmiSC.Name = "tsmiSC";
            this.tsmiSC.Size = new System.Drawing.Size(124, 22);
            this.tsmiSC.Text = "选课";
            // 
            // tsmiLoop
            // 
            this.tsmiLoop.Name = "tsmiLoop";
            this.tsmiLoop.Size = new System.Drawing.Size(124, 22);
            this.tsmiLoop.Text = "窗口遍历";
            this.tsmiLoop.Click += new System.EventHandler(this.tsmiLoop_Click);
            // 
            // tsmiCollectOp
            // 
            this.tsmiCollectOp.Name = "tsmiCollectOp";
            this.tsmiCollectOp.Size = new System.Drawing.Size(124, 22);
            this.tsmiCollectOp.Text = "集合操作";
            this.tsmiCollectOp.Click += new System.EventHandler(this.tsmiCollectOp_Click);
            // 
            // tsmiDict
            // 
            this.tsmiDict.Name = "tsmiDict";
            this.tsmiDict.Size = new System.Drawing.Size(124, 22);
            this.tsmiDict.Text = "字典操作";
            this.tsmiDict.Click += new System.EventHandler(this.tsmiDict_Click);
            // 
            // miWins
            // 
            this.miWins.Name = "miWins";
            this.miWins.Size = new System.Drawing.Size(44, 21);
            this.miWins.Text = "窗口";
            // 
            // tsmiView
            // 
            this.tsmiView.Name = "tsmiView";
            this.tsmiView.Size = new System.Drawing.Size(180, 22);
            this.tsmiView.Text = "DataViewStudent";
            this.tsmiView.Click += new System.EventHandler(this.tsmiView_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.msMain);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.msMain;
            this.Name = "FormMain";
            this.Text = "FormMain";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.msMain.ResumeLayout(false);
            this.msMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip msMain;
        private System.Windows.Forms.ToolStripMenuItem miSysMgr;
        private System.Windows.Forms.ToolStripMenuItem tsmiChangePass;
        private System.Windows.Forms.ToolStripSeparator tsmiSeparator1;
        private System.Windows.Forms.ToolStripMenuItem tsmiLogOff;
        private System.Windows.Forms.ToolStripMenuItem tsmiExit;
        private System.Windows.Forms.ToolStripSeparator tsmiSeparator2;
        private System.Windows.Forms.ToolStripMenuItem tsmiCalc;
        private System.Windows.Forms.ToolStripMenuItem miStuMgr;
        private System.Windows.Forms.ToolStripMenuItem tsmiStuBaseInfo;
        private System.Windows.Forms.ToolStripMenuItem miCourse;
        private System.Windows.Forms.ToolStripMenuItem tsmiCourse;
        private System.Windows.Forms.ToolStripMenuItem miSC;
        private System.Windows.Forms.ToolStripMenuItem tsmiSC;
        private System.Windows.Forms.ToolStripMenuItem miWins;
        private System.Windows.Forms.ToolStripMenuItem tsmiLoop;
        private System.Windows.Forms.ToolStripMenuItem tsmiCollectOp;
        private System.Windows.Forms.ToolStripMenuItem tsmiDict;
        private System.Windows.Forms.ToolStripMenuItem tsmiView;
    }
}