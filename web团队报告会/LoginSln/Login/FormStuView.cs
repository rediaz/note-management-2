﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Login
{
    public partial class FormStuView : Form
    {
        public FormStuView()
        {
            InitializeComponent();
        }

        private void FormStuView_Load(object sender, EventArgs e)
        {
            OleDbConnection oleDbConn = new OleDbConnection();
            oleDbConn.ConnectionString = "Provider=SQLOLEDB;Data Source=.;Integrated Security=SSPI;Initial Catalog=stu";
            //oleDbConn.Open();
            OleDbCommand oleDbCmd = oleDbConn.CreateCommand();
            oleDbCmd.CommandText = "select * from student";
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = oleDbCmd;
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            string filter = "sex = '男'";
            DataView dvMale = new DataView(dt, filter, "sno", DataViewRowState.CurrentRows);
            filter = "sex = '女'";
            DataView dvFemale = new DataView(dt, filter, "sno", DataViewRowState.CurrentRows);

            BindingSource bs = new BindingSource();           
            bs.DataSource = dvMale;
            dataGridView1.DataSource = bs;

            bs = new BindingSource();
            bs.DataSource = dvFemale;
            dataGridView2.DataSource = bs;
        }
    }
}
