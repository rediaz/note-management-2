﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Login
{
    public partial class FormCalc : Form
    {
        public FormCalc()
        {
            InitializeComponent();
        }

        private void btnNum_Click(object sender, EventArgs e)
        {                       
            Button btnNum = sender as Button;
            tbDisplay.Text = tbDisplay.Text + btnNum.Text;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            tbDisplay.Text = "";
        }

        private void btnOp_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            btnEqual.Tag = btn.Text;
            tbDisplay.Tag = tbDisplay.Text;
            tbDisplay.Text = "";
        }

        private void btnEqual_Click(object sender, EventArgs e)
        {
            int num1 = int.Parse(tbDisplay.Tag.ToString());
            int num2 = int.Parse(tbDisplay.Text);
            switch (((Button)sender).Tag.ToString())
            {
                case "+":
                    tbDisplay.Text = (num1 + num2).ToString();
                    break;
                case "-":
                    tbDisplay.Text = (num1 - num2).ToString();
                    break;
                case "*":
                    tbDisplay.Text = (num1 * num2).ToString();
                    break;
                case "/":
                    tbDisplay.Text = (num1 / num2).ToString();
                    break;
            }
        }
    }
}
