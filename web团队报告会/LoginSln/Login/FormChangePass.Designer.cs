﻿namespace Login
{
    partial class FormChangePass
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbPassOriginal = new System.Windows.Forms.Label();
            this.lbPassNew = new System.Windows.Forms.Label();
            this.lbPassConfirm = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.txtPassOriginal = new System.Windows.Forms.TextBox();
            this.txtPassNew = new System.Windows.Forms.TextBox();
            this.txtPassConfirm = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lbPassOriginal
            // 
            this.lbPassOriginal.AutoSize = true;
            this.lbPassOriginal.Location = new System.Drawing.Point(38, 31);
            this.lbPassOriginal.Name = "lbPassOriginal";
            this.lbPassOriginal.Size = new System.Drawing.Size(53, 12);
            this.lbPassOriginal.TabIndex = 0;
            this.lbPassOriginal.Text = "原始口令";
            // 
            // lbPassNew
            // 
            this.lbPassNew.AutoSize = true;
            this.lbPassNew.Location = new System.Drawing.Point(40, 79);
            this.lbPassNew.Name = "lbPassNew";
            this.lbPassNew.Size = new System.Drawing.Size(53, 12);
            this.lbPassNew.TabIndex = 1;
            this.lbPassNew.Text = "新 口 令";
            // 
            // lbPassConfirm
            // 
            this.lbPassConfirm.AutoSize = true;
            this.lbPassConfirm.Location = new System.Drawing.Point(40, 121);
            this.lbPassConfirm.Name = "lbPassConfirm";
            this.lbPassConfirm.Size = new System.Drawing.Size(53, 12);
            this.lbPassConfirm.TabIndex = 2;
            this.lbPassConfirm.Text = "确认口令";
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(42, 169);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 3;
            this.btnOk.Text = "确认";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(154, 169);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "关闭";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // txtPassOriginal
            // 
            this.txtPassOriginal.Location = new System.Drawing.Point(118, 31);
            this.txtPassOriginal.Name = "txtPassOriginal";
            this.txtPassOriginal.Size = new System.Drawing.Size(100, 21);
            this.txtPassOriginal.TabIndex = 5;
            // 
            // txtPassNew
            // 
            this.txtPassNew.Location = new System.Drawing.Point(118, 76);
            this.txtPassNew.Name = "txtPassNew";
            this.txtPassNew.Size = new System.Drawing.Size(100, 21);
            this.txtPassNew.TabIndex = 5;
            // 
            // txtPassConfirm
            // 
            this.txtPassConfirm.Location = new System.Drawing.Point(118, 112);
            this.txtPassConfirm.Name = "txtPassConfirm";
            this.txtPassConfirm.Size = new System.Drawing.Size(100, 21);
            this.txtPassConfirm.TabIndex = 5;
            // 
            // FormChangePass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(271, 223);
            this.Controls.Add(this.txtPassConfirm);
            this.Controls.Add(this.txtPassNew);
            this.Controls.Add(this.txtPassOriginal);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.lbPassConfirm);
            this.Controls.Add(this.lbPassNew);
            this.Controls.Add(this.lbPassOriginal);
            this.Name = "FormChangePass";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormChangePass";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbPassOriginal;
        private System.Windows.Forms.Label lbPassNew;
        private System.Windows.Forms.Label lbPassConfirm;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TextBox txtPassOriginal;
        private System.Windows.Forms.TextBox txtPassNew;
        private System.Windows.Forms.TextBox txtPassConfirm;
    }
}