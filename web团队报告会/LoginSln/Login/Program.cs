﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Login
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            FormLogin frmLogin = new FormLogin();
            int i = 0;
            OleDbConnection oleDbConn = new OleDbConnection();
            try
            {
                oleDbConn.ConnectionString = "Provider=SQLOLEDB;Data Source=.;Integrated Security=SSPI;Initial Catalog=stu";
                oleDbConn.Open();
                OleDbCommand oleDbCmd = new OleDbCommand();
                oleDbCmd.Connection = oleDbConn;

                for (; i < 3; i++)
                {
                    DialogResult dr = frmLogin.ShowDialog();
                    if (dr == DialogResult.OK)
                    {
                        string fmt = "select * from useraccount where username = '{0}' and userpassword = '{1}'";
                        string sql = string.Format(fmt, frmLogin.UserName, frmLogin.Password);
                        sql = "select * from useraccount where username = ? and userpassword = ?";
                        //oleDbCmd.CommandText = "select * from useraccount where username = '" +
                        //    frmLogin.UserName + "' and userpassword = '" + frmLogin.Password + "'";
                        oleDbCmd.Parameters.Clear();
                        oleDbCmd.CommandText = sql;
                        OleDbParameter param = new OleDbParameter();
                        param.Value = frmLogin.UserName;
                        oleDbCmd.Parameters.Add(param);

                        param = new OleDbParameter();
                        param.Value = frmLogin.Password;
                        oleDbCmd.Parameters.Add(param);

                        OleDbDataReader reader = oleDbCmd.ExecuteReader();
                        string strUser = frmLogin.GetUserName();
                        if (reader.HasRows)
                        {
                            Application.Run(new FormMain());
                            break;
                        }
                        else
                        {
                            MessageBox.Show("用户名或口令错误");
                        }
                        reader.Close();
                    }
                    else
                    {
                        break;
                    }
                }
                if (i >= 3)
                {
                    MessageBox.Show("账号已被锁定");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
