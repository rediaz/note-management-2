﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Login
{
    public partial class FormControlShow : Form
    {
        public FormControlShow()
        {
            InitializeComponent();            
        }

        private void FormControlShow_Load(object sender, EventArgs e)
        {
            if (this.Tag != null)
            {
                Form frm = this.Tag as Form;
                TreeNode tnRoot = new TreeNode(frm.Name);
                treeView1.Nodes.Add(tnRoot);
                RecursiveControl(frm, tnRoot);
                treeView1.ExpandAll();
            }
        }

        void RecursiveControl(Control ctlParent, TreeNode tnParent)
        {
            foreach (Control cc in ctlParent.Controls)
            {
                TreeNode tnChild = new TreeNode(cc.Name);
                tnChild.Tag = cc;
                tnParent.Nodes.Add(tnChild);
                RecursiveControl(cc, tnChild);
            }
        }

        private void tsmiDisabled_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode.Tag != null)
            {
                (treeView1.SelectedNode.Tag as Control).Enabled = false;
            }
        }
    }
}
