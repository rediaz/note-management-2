﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Login
{
    public partial class FormControls : Form
    {
        public FormControls()
        {
            InitializeComponent();
        }

        private void btnLoop_Click(object sender, EventArgs e)
        {
            TreeNode tnRoot = new TreeNode(this.Name);
            treeView2.Nodes.Add(tnRoot);
            //RecursiveControls
            foreach (Control c in this.Controls)
            {
                TreeNode tn = new TreeNode(c.Name);
                tnRoot.Nodes.Add(tn);
                //RecursiveControlss
                foreach (Control cc in c.Controls)
                {
                    TreeNode tnChild = new TreeNode(cc.Name);
                    tn.Nodes.Add(tnChild);
                    //RecursiveControlsss
                    foreach (Control ccc in cc.Controls)
                    {
                        TreeNode tnChildChild = new TreeNode(ccc.Name);
                        tnChild.Nodes.Add(tnChildChild);
                    }
                }
            }
            treeView2.ExpandAll();
        }

        private void btnRecursive_Click(object sender, EventArgs e)
        {
            TreeNode tnRoot = new TreeNode(this.Name);
            treeView1.Nodes.Add(tnRoot);
            RecursiveControl(this, tnRoot);
            treeView1.ExpandAll();
        }

        void RecursiveControl(Control ctlParent, TreeNode tnParent)
        {
            foreach (Control cc in ctlParent.Controls)
            {
                TreeNode tnChild = new TreeNode(cc.Name);
                tnParent.Nodes.Add(tnChild);
                RecursiveControl(cc, tnChild);
                //重点关注
                //RecursiveControls(cc, tnChild);
            }
        }

        void RecursiveControls(Control ctlParent, TreeNode tnParent)
        {
            foreach (Control cc in ctlParent.Controls)
            {
                TreeNode tnChild = new TreeNode(cc.Name);
                tnParent.Nodes.Add(tnChild);
                RecursiveControlss(cc, tnChild);
            }
        }

        void RecursiveControlss(Control ctlParent, TreeNode tnParent)
        {
            foreach (Control cc in ctlParent.Controls)
            {
                TreeNode tnChild = new TreeNode(cc.Name);
                tnParent.Nodes.Add(tnChild);
                RecursiveControlsss(cc, tnChild);
            }
        }

        void RecursiveControlsss(Control ctlParent, TreeNode tnParent)
        {
            foreach (Control ccc in ctlParent.Controls)
            {
                TreeNode tnChildChild = new TreeNode(ccc.Name);
                tnParent.Nodes.Add(tnChildChild);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            FormControlShow frmCtlShow = new FormControlShow();
            frmCtlShow.Tag = this;
            frmCtlShow.Show();
        }
    }
}
