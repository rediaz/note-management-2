﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace Login
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void tsmiChangePass_Click(object sender, EventArgs e)
        {
            FormChangePass frmChangePass = new FormChangePass();
            frmChangePass.ShowDialog();
        }

        private void tsmiLogOff_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void tsmiExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void tsmiCalc_Click(object sender, EventArgs e)
        {
            //Process.Start("calc.exe");
            FormCalc frmCalc = new FormCalc();
            frmCalc.Show();
        }

        private void tsmiStuBaseInfo_Click(object sender, EventArgs e)
        {
            //ShowForm(new FormBaseInfo((ToolStripMenuItem)sender), (ToolStripMenuItem)sender);
            //FormBaseInfo frmBaseInfo = new FormBaseInfo(tsmiStuBaseInfo);
            //frmBaseInfo.MdiParent = this;
            //frmBaseInfo.Show();
            //frmBaseInfo.Enabled = false;

            //ToolStripMenuItem tsmi = new ToolStripMenuItem(frmBaseInfo.Text);
            //tsmi.Click += Tsmi_Click;
            //miWins.DropDownItems.Add(tsmi);
            //frmBaseInfo.Tag = tsmi;
            //tsmi.Tag = frmBaseInfo;

            FormStuInfo frmStuInfo = new FormStuInfo();
            frmStuInfo.Show();
        }

 
        private void tsmiCourse_Click(object sender, EventArgs e)
        {
            ShowForm(new FormCourse((ToolStripMenuItem)sender), (ToolStripMenuItem)sender);
            //FormCourse frmCourse = new FormCourse(tsmiCourse);
            //frmCourse.MdiParent = this;
            //frmCourse.Show();
            //tsmiCourse.Enabled = false;

            //ToolStripMenuItem tsmi = new ToolStripMenuItem(frmCourse.Text);
            //tsmi.Click += Tsmi_Click;
            //miWins.DropDownItems.Add(tsmi);
            //frmCourse.Tag = tsmi;
            //tsmi.Tag = frmCourse;
        }

        void ShowForm(Form frm, ToolStripMenuItem tsmiFunc)
        {
            frm.MdiParent = this;
            frm.Show();
            tsmiFunc.Enabled = false;

            ToolStripMenuItem tsmi = new ToolStripMenuItem(frm.Text);
            tsmi.Click += Tsmi_Click;
            miWins.DropDownItems.Add(tsmi);
            frm.Tag = tsmi;
            tsmi.Tag = frm;
        }

        private void Tsmi_Click(object sender, EventArgs e)
        {
            if (sender is ToolStripMenuItem)
            {
                ToolStripMenuItem tsmi = sender as ToolStripMenuItem;
                if (tsmi.Tag != null)
                {
                    if (tsmi.Tag is Form)
                    {
                        Form frm = tsmi.Tag as Form;
                        frm.WindowState = FormWindowState.Normal;
                        frm.BringToFront();
                    }
                }
            }
        }

        private void tsmiLoop_Click(object sender, EventArgs e)
        {
            FormControls frmCtls = new FormControls();
            frmCtls.Show();
        }

        private void tsmiCollectOp_Click(object sender, EventArgs e)
        {
            FormCollection frmCollection = new FormCollection();
            frmCollection.Show();
        }

        private void tsmiDict_Click(object sender, EventArgs e)
        {
            FormDict frmDict = new FormDict();
            frmDict.Show();
        }

        private void tsmiView_Click(object sender, EventArgs e)
        {
            FormStuView frmView = new FormStuView();
            frmView.Show();
        }
    }
}
