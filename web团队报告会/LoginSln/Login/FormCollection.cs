﻿using System;
//using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Login
{
    public partial class FormCollection : Form
    {
        List<int> lst = null;
        public FormCollection()
        {
            InitializeComponent();
        }

        private void btnLstInit_Click(object sender, EventArgs e)
        {
            lst = new List<int>();
            btnAdd.Enabled = true;
            btnModified.Enabled = true;
            btnSearch.Enabled = true;
            btnRecursive.Enabled = true;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            lst.Add(1);
            lst.Add(2);
            lst.Add(3);
            lst.Add(5);
            lst.Add(5);
            lst.Add(5);
            lst.Add(3);
            lst.Add(5);
            lst.Add(5);
            lst.Add(5);
            for (int i = 0; i < lst.Count; i++)
            {
                Text = Text + lst[i].ToString() + "    ";
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            int i = 0;
            for (i = 0; i < lst.Count; i++)
            {
                if (lst[i].ToString() == textBox1.Text)
                {
                    DialogResult dr = MessageBox.Show("是否删除？", "提示", MessageBoxButtons.YesNo);
                    if (dr == DialogResult.Yes)
                    {
                        lst.RemoveAt(i);
                        MessageBox.Show("删除成功！");                       
                    }
                    break;
                }                   
            }
            if(i >= lst.Count)
            {
                MessageBox.Show("集合中无指定元素，请重新输入要删除的元素！");
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            btnRemove.Enabled = !string.IsNullOrEmpty(textBox1.Text);
        }

        private void btnModified_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(tbIndex.Text) && lst.Count > int.Parse(tbIndex.Text))
            {
                lst[int.Parse(tbIndex.Text)] = 10;
            }

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < lst.Count; i++)
            {
                if (lst[i].ToString() == tb2Search.Text)
                {
                    string fmt = "查找的元素值{0}，索引位置是{1}";
                    MessageBox.Show(string.Format(fmt, tb2Search.Text, i));
                }
            }
        }

        private void btnRecursive_Click(object sender, EventArgs e)
        {
            string strShow = null;
            for (int i = 0; i < lst.Count; i++)
            {
                strShow = strShow + lst[i].ToString() + "    ";
            }
            Text = strShow;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Text = "";
            for (int i = 0; i < lst.Count; i++)
            {
                if (lst[i].ToString() == textBox1.Text)
                {
                    lst.RemoveAt(i);
                }
            }

            foreach(int i in lst)
            {
                Text = Text + "   " + i.ToString();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Text = "";
            for (int i = lst.Count - 1; i >= 0; i--)
            {
                if (lst[i].ToString() == textBox1.Text)
                {
                    lst.RemoveAt(i);
                }
            }

            foreach (int i in lst)
            {
                Text = Text + "   " + i.ToString();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            foreach(int i in lst)
            {
                if (i.ToString() == textBox1.Text)
                {
                    lst.RemoveAt(i);
                }
            }
        }
    }
}
