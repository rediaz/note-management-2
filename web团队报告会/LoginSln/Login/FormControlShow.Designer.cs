﻿namespace Login
{
    partial class FormControlShow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiDisabled = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEnabled = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiHidden = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiShown = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeView1
            // 
            this.treeView1.ContextMenuStrip = this.contextMenuStrip1;
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Left;
            this.treeView1.Location = new System.Drawing.Point(0, 0);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(341, 450);
            this.treeView1.TabIndex = 0;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiDisabled,
            this.tsmiEnabled,
            this.tsmiHidden,
            this.tsmiShown});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(181, 114);
            // 
            // tsmiDisabled
            // 
            this.tsmiDisabled.Name = "tsmiDisabled";
            this.tsmiDisabled.Size = new System.Drawing.Size(180, 22);
            this.tsmiDisabled.Text = "禁用";
            this.tsmiDisabled.Click += new System.EventHandler(this.tsmiDisabled_Click);
            // 
            // tsmiEnabled
            // 
            this.tsmiEnabled.Name = "tsmiEnabled";
            this.tsmiEnabled.Size = new System.Drawing.Size(180, 22);
            this.tsmiEnabled.Text = "启用";
            // 
            // tsmiHidden
            // 
            this.tsmiHidden.Name = "tsmiHidden";
            this.tsmiHidden.Size = new System.Drawing.Size(180, 22);
            this.tsmiHidden.Text = "隐藏";
            // 
            // tsmiShown
            // 
            this.tsmiShown.Name = "tsmiShown";
            this.tsmiShown.Size = new System.Drawing.Size(180, 22);
            this.tsmiShown.Text = "显示";
            // 
            // FormControlShow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.treeView1);
            this.Name = "FormControlShow";
            this.Text = "FormControlShow";
            this.Load += new System.EventHandler(this.FormControlShow_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmiDisabled;
        private System.Windows.Forms.ToolStripMenuItem tsmiEnabled;
        private System.Windows.Forms.ToolStripMenuItem tsmiHidden;
        private System.Windows.Forms.ToolStripMenuItem tsmiShown;
    }
}