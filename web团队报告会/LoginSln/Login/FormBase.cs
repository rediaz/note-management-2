﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Login
{
    public partial class FormBase : Form
    {
        ToolStripMenuItem m_Tsmi;
        public FormBase()
        {
            InitializeComponent();
        }
        public FormBase(ToolStripMenuItem tsmi)
        {
            m_Tsmi = tsmi;
            InitializeComponent();
        }

        private void FormBase_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (m_Tsmi != null)
            {
                m_Tsmi.Enabled = true;
            }
            if (this.Tag != null)
            {
                if (this.Tag is ToolStripMenuItem)
                {
                    ToolStripMenuItem tsmi = this.Tag as ToolStripMenuItem;                    
                    ToolStripItem miParent = tsmi.OwnerItem;
                    if (miParent is ToolStripMenuItem)
                    {
                        (miParent as ToolStripMenuItem).DropDownItems.Remove(tsmi);
                    }
                }
            }
        }
    }
}
