﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Login
{
    public partial class FormStuInfo : Form
    {
        public FormStuInfo()
        {
            InitializeComponent();
        }

        private void FormStuInfo_Load(object sender, EventArgs e)
        {
            OleDbConnection oleDbConn = new OleDbConnection();
            oleDbConn.ConnectionString = "Provider=SQLOLEDB;Data Source=.;Integrated Security=SSPI;Initial Catalog=stu";
            oleDbConn.Open();
            OleDbCommand oleDbCmd = oleDbConn.CreateCommand();
            oleDbCmd.CommandText = "select * from student";
            OleDbDataReader reader = oleDbCmd.ExecuteReader();
            string fmt = "{0}   {1}     {2}    {3}   {4}";
            while (reader.Read())
            {
                string item = string.Format(fmt, reader["sno"], reader["sname"], reader["age"], reader["dept"], reader["sex"]);
                listBox1.Items.Add(item);
            }
            GetDataTable("select * from student", dataGridView1);

        }

        DataTable GetDataTable(string sql, DataGridView dgv)
        {
            OleDbConnection oleDbConn = new OleDbConnection();
            oleDbConn.ConnectionString = "Provider=SQLOLEDB;Data Source=.;Integrated Security=SSPI;Initial Catalog=stu";
            //oleDbConn.Open();
            OleDbCommand oleDbCmd = oleDbConn.CreateCommand();
            oleDbCmd.CommandText = sql;
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = oleDbCmd;
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            BindingSource bs = new BindingSource();
            bs.DataSource = dt;
            dgv.DataSource = bs;
            return dt;
        }

        private void button1_Click(object sender, EventArgs e)
        {
 

        }

        BindingSource GetBindingSourceByGridView(DataGridView dgv)
        {
            BindingSource rtn = null;
            if (dgv != null)
            {
                if (dgv.DataSource is BindingSource)
                {
                    rtn = dgv.DataSource as BindingSource;
                }
            }
            return rtn;
        }

        private void tsmDel_Click(object sender, EventArgs e)
        {
            BindingSource bs = GetBindingSourceByGridView(dataGridView1);
            bs.RemoveCurrent();
        }

        private void tsbDel_Click(object sender, EventArgs e)
        {
            tsmDel_Click(sender, new EventArgs());
        }

        private void tsbSave_Click(object sender, EventArgs e)
        {
            BindingSource bs = GetBindingSourceByGridView(dataGridView1);
            if (bs != null)
            {
                if (bs.DataSource is DataTable)
                {
                    OleDbConnection oleDbConn = new OleDbConnection();
                    oleDbConn.ConnectionString = "Provider=SQLOLEDB;Data Source=.;Integrated Security=SSPI;Initial Catalog=stu";
                    oleDbConn.Open();
                    OleDbCommand oleDbCmd = oleDbConn.CreateCommand();
                    oleDbCmd.CommandText = "delete from student where sno = ?";

                    try
                    {
                        DataTable dtChanged = (bs.DataSource as DataTable).GetChanges(DataRowState.Deleted);
                        if (dtChanged != null)
                        {

                            foreach (DataRow dr in dtChanged.Rows)
                            {
                                AddParams(oleDbCmd, dr["sno", DataRowVersion.Original]);
                                int RowAffected = oleDbCmd.ExecuteNonQuery();
                            }
                        }
                        dtChanged = (bs.DataSource as DataTable).GetChanges(DataRowState.Modified);
                        if (dtChanged != null)
                        {
                            oleDbCmd.CommandText = "update student set sno = ?, sname = ?, sex = ?, dept = ?, age = ? where sno = ?";

                            foreach (DataRow dr in dtChanged.Rows)
                            {
                                AddParams(oleDbCmd, dr["sno", DataRowVersion.Current], dr["sname", DataRowVersion.Current],
                                    dr["sex", DataRowVersion.Current], dr["dept", DataRowVersion.Current], 
                                    dr["age", DataRowVersion.Current], dr["sno", DataRowVersion.Original]);
                                int RowAffected = oleDbCmd.ExecuteNonQuery();
                            }
                        }

                        dtChanged = (bs.DataSource as DataTable).GetChanges(DataRowState.Added);
                        if (dtChanged != null)
                        {
                            oleDbCmd.CommandText = "insert into student values (?, ?, ?, ?, ?)";
                            foreach (DataRow dr in dtChanged.Rows)
                            {
                                AddParams(oleDbCmd, dr["sno", DataRowVersion.Current], dr["sname", DataRowVersion.Current], 
                                    dr["sex", DataRowVersion.Current], dr["dept", DataRowVersion.Current], dr["age", DataRowVersion.Current]);
                                int RowAffected = oleDbCmd.ExecuteNonQuery();
                            }
                        }
                        (bs.DataSource as DataTable).AcceptChanges();
                        MessageBox.Show("兄弟，恭喜您，数据操作成功！");
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    finally
                    {
                        oleDbConn.Close();
                    }
                }
            }
        }

        void AddParams(OleDbCommand cmd, params object[] vals)
        {
            if (cmd != null)
            {
                cmd.Parameters.Clear();
                foreach(object obj in vals)
                {
                    OleDbParameter param = new OleDbParameter();
                    param.Value = obj;
                    cmd.Parameters.Add(param);
                }
            }
        }
    }
}
