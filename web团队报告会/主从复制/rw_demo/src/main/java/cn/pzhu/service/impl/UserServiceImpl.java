package cn.pzhu.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.pzhu.entity.User;
import cn.pzhu.mapper.UserMapper;
import cn.pzhu.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper,User> implements UserService {
}
