package cn.pzhu.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.annotation.Order;

import javax.sql.DataSource;

@Configuration
@Order(1)
class DruidDataSourceAutoConfigure {
    @Bean
    public DataSource dataSource() {
        // 初始化并配置DruidDataSource，返回一个DataSource Bean
        return new DruidDataSource();
    }
}


