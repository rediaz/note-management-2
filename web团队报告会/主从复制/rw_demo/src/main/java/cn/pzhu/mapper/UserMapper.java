package cn.pzhu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.pzhu.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<User>{
}
