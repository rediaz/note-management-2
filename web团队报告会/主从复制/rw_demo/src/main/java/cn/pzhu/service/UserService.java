package cn.pzhu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.pzhu.entity.User;

public interface UserService extends IService<User> {
}
