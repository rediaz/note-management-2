# 主从复制

[TOC]

## 一、什么是主从复制？

主从复制是一种<u>数据库复制技术</u>，其中一个数据库被定义为**主数据库（Master）**，而其他一个或多个数据库被定义为**从数据库（Slave）**。主数据库负责处理**写**操作，并将这些写操作记录在称为**二进制日志（Binary Log）**的日志文件中。从数据库通过读取主数据库的二进制日志，并将这些写操作应用到自身的数据库中，从而与主数据库保持*同步*。



## 二、主从复制的作用

1、架构的扩展（**高可用性**）：业务量越来越大，I/O访问频率过高，单机无法满足，此时做多库的存储，物理服务器增加，负荷增加。

如果主服务器出现故障，则从服务器可以自动接管。

2、读写分离（**负载均衡**）：使数据库能支撑更大的并发。<u>主从只负责各自的写和读</u>，极大程度的缓解X锁和S锁争用。在报表中尤其重要。由于部分报表SQL语句非常的慢，导致锁表，影响前台服务。如果前台使用master，报表使用slave，那么报表sql将不会造成前台锁，保证了前台速度。

3、数据的**备份**：作为后备数据库，主数据库服务器故障后，可切换到从数据库继续工作，*<u>避免数据丢失</u>*。



## 三、常见应用主从复制的中间件

1. Redis：Redis是一个开源的内存数据结构存储系统，支持主从复制。它使用发布/订阅模式来同步数据，并且可以实现高可用性和读写分离。

2. MySQL：MySQL是一个流行的关系型数据库管理系统，也支持主从复制。通过配置主从复制，可以将数据从主数据库复制到一个或多个从数据库，实现数据的备份和读写分离。

3. PostgreSQL：PostgreSQL是另一个流行的关系型数据库管理系统，也支持主从复制。它使用流复制技术来复制数据，并且可以实现高可用性和读写分离。

4. MongoDB：MongoDB是一个面向文档的NoSQL数据库，也支持主从复制。它使用复制集来实现数据的复制和故障恢复，同时支持读写分离。

5. Apache Kafka：Apache Kafka是一个分布式流处理平台，也可以用作主从复制的中间件。它使用副本机制来复制数据，并且具有高吞吐量和可扩展性。

 这些中间件都提供了主从复制的功能，可以根据具体的需求选择适合的中间件来实现数据的复制和读写分离。



## 四、Mysql的主从复制

MySQL主从复制是一个异步的复制过程，底层是基于MysqI数据库自带的**二进制日志**功能。就是<u>一台或多台</u>MySQL数据库(slave，即**从库**)从另一台MySQL数据库(master，即**主库**）进行日志的复制然后再解析日志并应用到自身，最终实现**从库的数据**和**主库的数据**保持<u>一致</u>。MySQL主从复制是MySQL数据库自带功能，无需借助第三方工具。

> 扩展：mysql中的日志分类一般有如下三种：
>
> ![mysql主主复制优缺点 mysql主从复制的作用_数据库](https://gitee.com/mannor/resouces/raw/master/PicGo/04180521_647c61e1a94d489289.png)

MySQL复制过程分成三步:

- master将改变记录到二进制日志(binary log)
- slave将master的binary log拷贝到它的中继日志（relay log)
- slave重做中继日志中的事件，将改变应用到自己的数据库中

Mysql主从复制图解：

![image.png](https://gitee.com/mannor/resouces/raw/master/PicGo/1698478310159-9640c9fc-de3e-43b7-b682-1e8522f1087c.png)

> 在dump线程检测到binlog变化时,会从指定位开始读取内容，并把它发送给从服务器的I/O线程
> 这里需要注意，有些资料上面说这里是主服务器向从服务器推的，但是，实际上是从服务器向主服务器拉的。
>
> 从服务器连接到主服务器，请求从指定的位置开始获取binlog文件。
>
> 从服务器在日志中读取最后一次成功更新的位置，并接收从那时起发生的任何更新，然后封锁并等待主服务器通知下一次更新。
>
> MySQL主从复制涉及以下几个线程：
>
> 1. 主库的binlog dump线程：负责将主库的binlog日志发送给从库。
> 2. 从库的I/O线程：负责从主库获取binlog日志，并将日志写入从库的relay log中。
> 3. 从库的SQL线程：负责读取从库的relay log，并执行其中的SQL语句，将数据同步到从库中。
> 4. 主库的复制线程：负责将主库的数据变更同步到从库。

## 五、Mysql主从的形式

1. 一主一从：

   ![mysql主主复制优缺点 mysql主从复制的作用_服务器_03](https://gitee.com/mannor/resouces/raw/master/PicGo/04180521_647c61e1ab62542729.png)

2. 主主复制：

   ![mysql主主复制优缺点 mysql主从复制的作用_mysql_04](https://gitee.com/mannor/resouces/raw/master/PicGo/04180521_647c61e1aa3a944876.png)

3. 一主多从：

   ![mysql主主复制优缺点 mysql主从复制的作用_数据库_05](https://gitee.com/mannor/resouces/raw/master/PicGo/04180521_647c61e1e249423462.png)

4. 多主一从：

   ![mysql主主复制优缺点 mysql主从复制的作用_MySQL_06](https://gitee.com/mannor/resouces/raw/master/PicGo/04180521_647c61e1ba7422422.png)

   5. 联级复制：

      ![mysql主主复制优缺点 mysql主从复制的作用_mysql_07](https://gitee.com/mannor/resouces/raw/master/PicGo/04180521_647c61e1a948174364.png)

> 上述图片来自：https://blog.51cto.com/u_12902/6411632

## 六、Mysql主从复制的配置

[Mysql主从复制的配置CSDN文档](https://blog.csdn.net/m0_63144319/article/details/132677338)

### 6.1、前置条件

提前准备好两台服务器，分别安装Mysql并启动服务成功

- 主库Master 192.168.12.131
- 从库Slave 192.168.12.133

> ip以自己的虚拟机或者本地机而不同

### 6.2、配置-主库Master

第一步：修改Mysql数据库的配置文件：/etc/my.cnf

> windows：`D:\ProgramData\MySQL\MySQL Server 8.0\my.ini`

~~~~shell
[mysqld]
log-bin=mysql-bin         #[必须]启用二进制日志
server-id=100             #[必须]服务器唯一ID
~~~~

> 必须明确的指定一个唯一的服务器ID,默认服务器ID通常为1,使用默认值可能会导致和其他服务器ID冲突，因此这里我们选择10来作为服务器ID。

第二步：重启Mysql服务

~~~shell
systemctl restart mysqld
~~~

第三步：登录mysql数据库，执行下面的SQL

~~~sql
GRANT REPLICATION SLAVE ON *.*to 'xiaoming'@'%' identified by 'Root@123456';
~~~

> 注:上面SQL的作用是创建一个用户**xiaoming**，密码为**Root@123456**，(用户名和密码可以自行定义)，并且给xiaoming用户授予**REPLICATION SLAVE**权限。常用于建立复制时所需要用到的用户权限，也就是slave必须被master授权具有该权限的用户，才能通过该用户复制。

第四步：登录Mysql数据库，执行下面SQL，记录下结果中**File**和**Position**的值

~~~sql
show master status;
~~~

![image-20230904200411299](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20230904200411299.png)

> 注:上面SQL的作用是查看Master的状态，执行完此SQL后不要再执行任何操作

### 6.3、配置-从库

第一步：修改Mysql数据库的配置文件/etc/my.cnf

~~~shell
[mysqld]
server-id=101 #[必须]服务器唯一ID
~~~

第二步：重启Mysql服务

~~~shell
systemctl restart mysqld
~~~

第三步：登录mysql数据库，执行下面的SQL

~~~sql
change master to master_host='192.168.12.131',master_user= 'xiaoming',master_password='Root@123456',master_log_file='mysql-bin.000029',master_log_pos=1565;
~~~

> 将上述的各个参数信息填写完整正确，master_host、master_user、master_password、master_log_file，master_log_pos，其中，最后两个我们在从库中使用`show master status;`命令查询到。

执行完成之后，启动slave：

~~~sql
start slave;
~~~

![image-20230904202059044](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20230904202059044.png)

第四步：登录Mysql数据库，执行下面SQL，查看从数据库的状态

~~~sql
show slave status;
~~~

![image-20230904202335093](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20230904202335093.png)

如同上述，不方便观看，可以放到notepad中查看：

![image-20230904202503489](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20230904202503489.png)

下面标红的选项是yes，那就说明主从复制设置成功。

![image-20230904202815456](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20230904202815456.png)

> 注意查看其中file和 position是否和showslave status中的master_log_file 和 master_log_pos匹配。如果不一致则在从数据库(192.168.1.2)中执行以下语句，以设定读取的日志和位置
>
> 1. slave stop；
> 2. change master to master_log_file='log.000031'，master_log_pos=75；
> 3. slave start；
>    执行完毕后，再次查看

### 6.4、测试

 打开数据库管理工具（下面的是navicat），并在主库中创建数据库，并且对表进行设计，查看从库的状态，刷新后数据一致就说明主从复制的操作已经成功了。![image-20230904203729080](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20230904203729080.png)

## 七、主从复制方式--异步和半同步

MySQL 中主要有以下两种主从复制的模式，分别是异步复制和半同步复制。

1. **异步复制**

   ：MySQL 主从复制中最常见和默认的模式。在异步复制模式中，主服务器将数据修改操作记录到二进制日志（Binary Log）中，并将日志传输给从服务器。从服务器接收到二进制日志后，会异步地应用这些日志进行数据复制。

   1. 优点：它的优点是及时响应给使用者，主服务器不会受到从服务器的影响而等待确认，可以提高主服务器的性能。
   2. 缺点：由于是异步复制，可能存在数据传输的延迟，且从服务器上的复制过程是不可靠的。如果主服务器故障，尚未应用到从服务器的数据可能会丢失。

2. **半同步复制**

   ：半同步复制是 MySQL 主从复制中的一种增强模式。在半同步复制模式中，主服务器将数据修改操作记录到二进制日志，并等待至少一个从服务器确认已接收到并应用了这些日志后才继续执行后续操作。

   1. 优点：可以提供更高的数据一致性和可靠性，确保至少一个从服务器与主服务器保持同步。如果主服务器故障，已经确认接收并应用到从服务器的数据不会丢失。
   2. 缺点：由于半同步复制需要等待从服务器的确认，因此相对于异步复制，会增加一定的延迟，可能会影响主服务器的性能。

如果对数据一致性和可靠性要求较高，可以考虑使用半同步复制；如果对延迟和主服务器性能要求较高，可以继续使用异步复制，根据实际需求调整复制模式。

## 八、Mysql实现读写分离

### 背景

- 面对日益增加的系统访问量，数据库的吞吐量面临着巨大瓶颈。对于同一时刻有大量并发读操作和较少写操作类型的应用系统来说，将数据库拆分为**主库**和**从库**，主库负责处理事务性的增删改操作，从库负责处理查询操作，能够有效的避免由数据更新导致的行锁，使得整个系统的查询性能得到极大的改善。

![image-20230904204834688](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20230904204834688.png)

### 7.1、Sharding-JDBC介绍

Sharding-JDBC定位为**轻量级Java框架**，在Java的**JDBC层**提供的额外服务。它使用客户端直连数据库，以jar包形式提供服务，无需额外部署和依赖，可理解为增强版的JDBC驱动，完全兼容JDBC和各种ORM框架。
使用Sharding-JDBC可以在程序中轻松的实现数据库读写分离。

- 适用于任何基于JDBC的ORM框架，如:JPA, Hibernate,Mybatis, Spring JDBC Template或直接使用JDBC。
- 支持任何第三方的数据库连接池，如:DBCP,C3PO,BoneCP, Druid, HikariCP等。
- 支持任意实现[DBC规范的数据库。目前支持MySQL，Oracle，SQLServer，PostgreSQL以及任何遵循SQL92标准的数据库。

### 7.2、读写分离的实现

使用Sharding-JDBC实现读写分离步骤:

1、导入maven坐标

```xml
<dependency>
    <groupId>org.apache.shardingsphere</groupId>
    <artifactId>sharding-jdbc-spring-boot-starter</artifactId>
    <version>4.0.0-RC1</version>
</dependency>
```

2、在配置文件中配置读写分离规则

~~~yml
spring:
  shardingsphere:
    datasource:
      names:
        master,slave
      # 主数据源
      master:
        type: com.alibaba.druid.pool.DruidDataSource
        driver-class-name: com.mysql.cj.jdbc.Driver
        url: jdbc:mysql://192.168.12.131:3306/rw?characterEncoding=utf-8
        username: root
        password: toor
      # 从数据源
      slave:
        type: com.alibaba.druid.pool.DruidDataSource
        driver-class-name: com.mysql.cj.jdbc.Driver
        url: jdbc:mysql://192.168.12.133:3306/rw?characterEncoding=utf-8
        username: root
        password: toor
    masterslave:
      # 读写分离配置
      load-balance-algorithm-type: round_robin #轮询
      # 最终的数据源名称
      name: dataSource
      # 主库数据源名称
      master-data-source-name: master
      # 从库数据源名称列表，多个逗号分隔
      slave-data-source-names: slave
    props:
      sql:
        show: true #开启SQL显示，默认false，即 控制台可以输出sql
~~~

3、在配置文件中配置**允许bean定义覆盖**配置项

```yml
spring:
  main:
    allow-bean-definition-overriding: true  #允许been定义覆盖，默认值为false
```

> 未开启报错如下:
>
> ~~~java
> ***************************
> APPLICATION FAILED TO START
> ***************************
> 
> Description:
> 
> The bean 'dataSource', defined in class path resource [org/apache/shardingsphere/shardingjdbc/spring/boot/SpringBootConfiguration.class], could not be registered. A bean with that name has already been defined in class path resource [com/alibaba/druid/spring/boot/autoconfigure/DruidDataSourceAutoConfigure.class] and overriding is disabled.
> 
> Action:
> 
> Consider renaming one of the beans or enabling overriding by setting spring.main.allow-bean-definition-overriding=true
> ~~~



### 7.3、测试 

执行查询操作，断点运行：

![image-20230904214142787](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20230904214142787.png)

查询的是从库的数据：

![image-20230904214418175](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20230904214418175.png)

执行添加操作的时候，更改的是主库的数据：

![image-20230904215112686](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20230904215112686.png)

自此，我们便完成了基于Mysql主从复制的读写分离操作。

> 测试项目源码链接：[读写分离实现案例的源码](https://gitee.com/rediaz/note-management/tree/master/%E5%90%8E%E7%AB%AF%E6%8A%80%E6%9C%AF%E6%A0%88/Mysql/rw_demo)

