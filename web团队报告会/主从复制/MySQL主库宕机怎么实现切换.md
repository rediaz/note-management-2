使用MHA（Master High Availability）工具来管理MySQL主从复制，并实现自动故障转移。以下是使用MHA进行主从切换的详细步骤：

1. 在两台服务器上安装MHA工具：

   - 在每台MySQL服务器上安装MHA工具包，可以通过以下命令下载并安装：

     ```
     wget https://github.com/yoshinorim/mha4mysql-node/releases/download/v0.58/mha4mysql-node_0.58-0_all.deb
     dpkg -i mha4mysql-node_0.58-0_all.deb
     ```

2. 配置MHA的参数文件：

   - 在其中一台服务器上创建MHA的配置文件

     ```
     /etc/mha/manager.cnf
     ```

     ，配置文件内容如下：

     ```
     [server default]
     manager_workdir=/var/log/mha
     manager_log=/var/log/mha/manager.log
     
     [server1]
     hostname=192.168.12.131
     candidate_master=1
     
     [server2]
     hostname=192.168.12.133
     candidate_master=1
     
     [client]
     user=mha
     password=mhapassword
     ```

3. 启动MHA监控程序：

   - 在MHA管理服务器上启动MHA监控程序，使用以下命令：

     ```
     masterha_manager --conf=/etc/mha/manager.cnf
     ```

4. 监控主从复制状态：

   - MHA会自动监控MySQL主从复制的状态，并在主库宕机时触发自动故障转移。

5. 手动切换主从角色：

   - 如果需要手动切换主从角色，可以使用MHA提供的工具

     ```
     masterha_master_switch
     ```

     ，使用以下命令进行手动切换：

     ```
     masterha_master_switch --conf=/etc/mha/manager.cnf --master_state=alive --new_master_host=192.168.12.133
     ```

6. 验证主从切换：

   - 在切换完成后，可以通过查看MHA的日志文件和MySQL的主从状态来验证主从切换是否成功。







# 1.MHA概述 

​    MHA（Master High Availability）目前在MySQL高可用方面是一个相对成熟的解决方案，它由日本DeNA公司youshimaton（现就职于Facebook公司）开发，是一套优秀的作为MySQL高可用性环境下故障切换和主从提升的高可用软件。在MySQL故障切换过程中，MHA能做到在0~30秒之内自动完成数据库的故障切换操作，并且在进行故障切换的过程中，MHA能在最大程度上保证数据的一致性，以达到真正意义上的高可用。

该软件由两部分组成：MHA Manager（管理节点）和MHA Node（数据节点）。MHA Manager可以单独部署在一台独立的机器上管理多个master-slave集群，也可以部署在一台slave节点上。MHA Node运行在每台MySQL服务器上，MHA Manager会定时探测集群中的master节点，当master出现故障时，它可以自动将最新数据的slave提升为新的master，然后将所有其他的slave重新指向新的master。整个故障转移过程对应用程序完全透明。

在MHA自动故障切换过程中，MHA试图从宕机的主服务器上保存二进制日志，最大程度的保证数据的不丢失，但这并不总是可行的。例如，如果主服务器硬件故障或无法通过ssh访问，MHA没法保存二进制日志，只进行故障转移而丢失了最新的数据。使用MySQL 5.5的半同步复制，可以大大降低数据丢失的风险。MHA可以与半同步复制结合起来。如果只有一个slave已经收到了最新的二进制日志，MHA可以将最新的二进制日志应用于其他所有的slave服务器上，因此可以保证所有节点的数据一致性。

​    MHA工作原理总结为以下几条：

（1）从宕机崩溃的master保存二进制日志事件（binlog events）;

（2）识别含有最新更新的slave;

（3）应用差异的中继日志(relay log)到其他slave;

（4）应用从master保存的二进制日志事件(binlog events);

（5）提升一个slave为新master;

（6）使用其他的slave连接新的master进行复制。

​    本文主要讲述生产线上MHA安装部署步骤，以及生产线上MHA安装部署规范。

# 安装MHA步骤

安装部署要求先安装MySQL一主多从架构，MySQL主从部署此处不作详述。

## 安装前环境准备

apt-get install -y libdbd-mysql-perl libconfig-tiny-perl liblog-dispatch-perl libparallel-forkmanager-perl libmodule-install-perl

apt-get install -y perl-modules

cpan Module::Install

以上3步骤安装MHA相关依赖包。

## 配置ssh免密登录

MHA需要通过管理机名密登录到其它主机实现相关操作，因此需要配置各主机间的名密登录。包括：管理机能免密登录到主、从机，主从库之前能名密登录。

执行ssh-keygen，按下回车，即可生成公钥。

cd /root/.ssh

cat id_rsa.pub 

查看生成公钥。

​       然后将相关内容追加至远程主机/root/.ssh/authorized_keys文件下。

​       执行ssh 10.1.8.70查看登录是否正常。然后根据需要，配置各主机间名密登录。



## 安装manager

安装manager需要下载MHA软件包，但源码包：

/usr/local/share/perl/5.18.2/MHA/DBHelper.pm

/usr/local/share/perl/5.18.2/MHA/HealthCheck.pm

两个文件有bug，导致MHA连接数据库失败而造成数据库切换失败。

需要修改HealthCheck.pm文件第98行：

将  "DBI:mysql:;host=[$self->{ip}];" 修改成：

"DBI:mysql:;host=$self->{ip};"

即将中括号”[]”去掉，否则数据库连接会失败。

同样，DBHelper.pm文件，158 198现行需要同样作出修改。

以下给出修改后的源码包：

解压安装

tar –xzvf mha4mysql-manager-master-debug.tar

cd mha4mysql-manager-master

执行：perl Makefile.PL

make && make install

## 安装node

根据2.1节所述安装相关perl模块。然后上传node包：

解压：unzip mha4mysql-node-master-0.57.zip

cd mha4mysql-node-master

perl Makefile.PL

make && make install

## 2.5数据库权限配置

​    1.配置同步权限：从库也需要给主库相关的权限：

GRANT REPLICATION SLAVE ON *.* TO 'repuser'@'10.1.8.71' IDENTIFIED BY PASSWORD '*AAB87B764AC144C101917F99B63F1B2A46B41D1C' ;

2.主从库给MHA管理机root权限：

​    create user 'hauser'@'10.1.8.72' identified by PASSWORD '*87EB1403CFDEF808C504AE04319E580EF2177C18';

GRANT ALL PRIVILEGES ON *.* TO 'hauser'@'10.1.8.72' ;

  MHA配置文件

## 主要config文件参数

[server default]

manager_workdir=/data/mha/worddir  ##manager工作目录

remote_workdir=/data/mha/remote/  ##保存远程binlog目录

manager_log=/data/mha/log/managerlog/mha-armdb.log ##MHA日志

ssh_user=root    ##连接远程所用用户

ssh_port=22     ##ssh端口

repl_user=repuser  ##复制user

repl_password=repuser21cn  ##复制密码

\#multi_tier_slave=1

ping_interval=1 

ping_type=CONNECT

master_ip_failover_script="/opt/MHA/worddir/armdb/master_ip_failover  --orig_master_ssh_port=22"   ###自动切换脚本

\#master_ip_online_change_script=/opt/MHA/mha_work_dir/master_ip_online_change 

\#secondary_check_script=masterha_secondary_check -s 192.168.76.134 --user=root   ##备机作切换，防止脑裂（此处由于资源限制没有设置）

[server1]

user=hauser  ##MHA远程登录MySQLoot权限

password=xxxxx  ##MHA远程登录MySQL

candidate_master=1  ##是否可以选为主

ignore_fail=1    

check_repl_delay = 1

hostname=10.1.8.70

\#ip=10.1.8.70

port=3301

master_binlog_dir=/data/mysql/mysql3301/logs/binlog/  ##MySQLbinlog位置

[server2]

user=hauser

password=xxx

candidate_master=1

ignore_fail=1

check_repl_delay = 1

hostname=10.1.8.71

\#ip=10.1.8.71

port=3301

master_binlog_dir=/data/mysql/mysql3301/logs/binlog/

自动切换脚本

自动切换脚本详见附件，此处需要注意的是：

my $vip = '10.1.8.74/24';

my $key = "2";

my $ssh_start_vip = "/sbin/ifconfig eth0:$key $vip";

my $ssh_stop_vip = "/sbin/ifconfig eth0:$key down";

切换脚本需要修改VIP，其中VIP需要指定子网掩码。

## 运行MHA

nohup /usr/local/bin/masterha_manager  --conf=/opt/MHA/worddir/armdb/mha-armdb.cnf  --ignore_last_failover < /dev/null > /opt/MHA/worddir/armdb/manager.log 2>&1 &

MySQL relay log purge

1.数据库新增权限：

grant super,reload on *.* to hauser@10.1.8.70 identified by xxxxxxxx;

grant select on mysql.slave_relay_log_info to hauser@10.1.8.70 identified by 'cisnewLYMwnL';

\2. 改/usr/local/bin/purge_relay_logs

/usr/local/bin/purge_relay_logs源码(目前 只发现0.57有这个BUG,需要按下面的方法修改一下)

将第178行: my $dsn = "DBI:mysql:;host=[$opt{host}];port=$opt{port}";

改成: my $dsn = "DBI:mysql:;host=$opt{host};port=$opt{port}";

3.建脚本

cat>/data/pufan/scripts/clearRelay

\#!/usr/bin/perl

use 5.10.1;

use strict;

use POSIX;

use File::Path qw( mkpath );

my $user="hauser";

my $passwd="xxxxxx";

my $host="10.1.8.71";

my $port=3301;

my $purge="/usr/local/bin/purge_relay_logs";

my $logPath="/data/mha/arm/logs";

mkpath "$logPath/tmpRelayLog" ||die;

my $year_month_day=strftime("%Y%m%d",localtime());

open ERROR,'>>',"$logPath/purge_relay_logs.log.$year_month_day" or die "$!\n";

!system "$purge --user=$user --password=$passwd --host=$host --port=$port --workdir=$logPath/tmpRelayLog --disable_relay_log_purge >> $logPath/purge_relay_logs.log.$year_month_day 2>&1" or print ERROR "\n purge error !\n";

加入crontab

\#MHA clear mysql relay log

0 6 * * * perl /data/pufan/scripts/clearRelay &

生产部署规范

本章主要说明MHA生产部署规范，主要包括各目录规范；权限管理规范；配置文件规范等。

目录规范

配置文件目录：

/opt/MHA/worddir/

该目录下，根据不同项目，创建不同目录及配置文件，以arm为例，配置文件及配置目录为：/opt/MHA/worddir/armdb/mha-armdb.cnf

数据目录：/data/mha

log：保存日志文件

remote：保存远程binlog文件

worddir：mha工作目录

权限管理规范

1.配置文件有比较敏感信息，需要设置权限位为600。

2.mha管理数据库用户hauser需要指定给mha主机权限，不能出现通配符。

运行规范

统一放后台运行：

nohup /usr/local/bin/masterha_manager  --conf=/opt/MHA/worddir/armdb/mha-armdb.cnf  --ignore_last_failover < /dev/null > /opt/MHA/worddir/armdb/manager.log 2>&1 &

切换测试

部署好MHA后，关掉主库，观察切换情况，查看日志输出。若切换失败，查看日志并排查原因。



















![image-20240320151628987](https://gitee.com/mannor/resouces/raw/master/PicGo/image-20240320151628987.png)







或是直接还原初始快照（强列建议还原初始快照）

yum remove Percona-XtraDB-Cluster* httpd* php* Percona-Server*mariadb*-yyum remove mysql-community-server -yrm-fr/var/log/mysqld.log/var/lib/mysql/*yum clean all && yum makecache

#2.在node01.node02node03 安装mysql-community-5.7

yum -y install mysql-community-server

#数据库配置文件已经放在gw（如果没有请自行上传）

node01node02node03执行如下命令：

wget http://10.15.200.8/filesfmha/hostname-s.my.cnf-0/etc/my.cnf

#3.在所有节点安装MHA（node01node02node03node05）yum install mha4mysql-node-0.58 -y

主节点：node01安装数据库启动服务配置用户名和密码[root@node01~]#rm-fr/var/lib/mysql/*&& mysqld --initialize&& chown -Rmysql:mysql/var/lib/mysqlsystemctl start mysqldmysql -uroot -p`grep 'temporary password'/var/log/mysqld.log| awk'{print $11}

#修改root密码创建用户复制用户repl及mha管理用户mhamysql>ALTERUSER'root'@'localhost'IDENTIFIED BY'!@

#qweASD69';grant replication slave on *.* to repl@'10.15.200.%' identified by '!@#qweASD69';grant all on *.* to mha@'10.15.200.%' identified by '!@#qweASD69';flush privileges;node02：同理类似rm-fr /var/lib/mvsal/*&&mysald--initialize--user=mvsal







MHA 在应用时, 其原理是当检测到 master 主机发生异常时, 随即执行各个 slave 数据库 I/O 线程读取的master binlog 位置比对, 选择其中读取位置最近的 slave标记为 latest slave, 然后再 latest slave 与其它 slave 进行日志差异比对, 形成差异中继日志 relay log。 在完成 re-lay log 生成后, 在 latest slave 上执行故障数据库 master上提取的 binary log 日志, 并将此 latest slave 设置为新的 master。 在新 master 设置后, 在各个 slave 上首先执行差异中继日志 relay log, 然后重新指南新 master 即可完成新的主从复制关联修复, 具体执行步骤如下：
（1） 从故障 master 数据库中提取 binary log events事件。
（2） 完 成对 slave 分 析, 并 确 定 包 含 最 新 事 件 的slave, 记为 latest slave。
（3） 分析latest slave 与各 slave 之间的 relay log, 并将 relay log 应用到其他 slave 中。
（4） 将 原 master 的 binary log events 在 latest slave中执行, 并将 latest slave 设置为新 master, 最后记录日志文件 binlog file 和执行位置 position。
（5） 将其他 slave 从数据库指向到新 master 主库进行复制, 最后关闭 MHA manager 进程







（1） 安装 MHA
安装公共资源, 执行命令：
~~~shell
yum -y install perl-DBD-MySQL perl-Config-Tiny perl-Log-Dispatch perl-Parallel-ForkManager perl-Time-HiRes
~~~

安装数据节点, 执行命令：
~~~shell
yum -y --nogpgcheck install mha4mysql-node-0.57-0.el7.noarch.rpm
~~~

安装管理节点, 执行命令：

~~~shell
yum -y install --nogpgcheck mha4mysql-manager-0.57-0.el7.noarch.rpm
~~~



（2） SSH 互信
为了保障 master 在故障时, MHA manager 管理节点
还能取得二进制日志, 需要配置其 SSH 互信, 其中在
master 与 slave 上分别执行：
ssh-keygen -t rsa
rm -rf ~/.ssh/*
在 manager 上执行：
ssh-keygen -t rsa
cd ~/.ssh
mv id rsa.pub authorized keys
scp * 10.1.1.110:~/.ssh/
scp * 10.1.1.111:~/.ssh/
（3） MHA 配置
为了完成 MHA 节点的自主管理, 需要事先对各个
节点进行配置管理, 如下所示：
[server defau|t]
user=mmm
password=123321
manager workdir=/mha/test
manager |og=/mha/test/test.|og
remote workdir=/mha/test
ssh user=mysq|
rep| user=rep|
rep| password=123456
ping interva|=3
//检查、切换、关闭脚本
secondary check script = "masterha secondary check
-s 10.1.1.122 -s 10.1.1.122"
master ip fai|over script = "/etc/mha/mas-
ter ip fai|over.sh 10.1.1.131 1"
master ip on|ine change script = "/etc/mha/mas-
ter ip on|ine change.sh 10.1.1.131 1"
shutdown script="/etc/mha/power manager"
//服务 1
[server1]
hostname=10.1.1.14
port=3306
master bin|og dir=/mha/db
candidate master=1
master pid fi|e=/mha/db/mysq|.pid
//服务 2
[server2]
hostname=10.1.1.15
port=3306
master bin|og dir=/mha/db
candidate master=1
master pid fi|e=/mha/db/mysq|.pid
//二进制日志存储
[bin|og1]
hostname=10.1.1.122
master bin|og dir=/mha/bin|og
no master=1
ignore fai|=1
（4） 启动 MHA manager 进程
在完成 MHA 核心配置后, 首先要对配置信息进行
验证, 其中验证 SSH 通信执行命令：
masterha check ssh --conf=/etc/mha/mha.cnf
验证复制执行命令：
masterha check rep| --conf=/etc/mha/mha.cnf
(下转第 89 页)
60
万方数据













