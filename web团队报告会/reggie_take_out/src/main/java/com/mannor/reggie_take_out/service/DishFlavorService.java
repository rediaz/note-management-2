package com.mannor.reggie_take_out.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mannor.reggie_take_out.dto.DishDto;
import com.mannor.reggie_take_out.entity.DishFlavor;


public interface DishFlavorService extends IService<DishFlavor> {

}
