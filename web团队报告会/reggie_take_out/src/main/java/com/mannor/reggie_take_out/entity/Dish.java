package com.mannor.reggie_take_out.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 菜品
 */
@Data
public class Dish implements Externalizable {
//将上述Serializable替换成Externalizable
    private static final long serialVersionUID = -2124026633541070753L;


    private Long id;

    //菜品名称
    private String name;


    //菜品分类id
    private Long categoryId;


    //菜品价格
    private BigDecimal price;


    //商品码
    private String code;


    //图片
    private String image;


    //描述信息
    private String description;


    //0 停售 1 起售
    private Integer status;


    //顺序
    private Integer sort;


    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;


    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


    @TableField(fill = FieldFill.INSERT)
    private Long createUser;


    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser;


    //是否删除
    private Integer isDeleted;

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(id);
        out.writeObject(name);
        out.writeObject(categoryId);
        out.writeObject(price);
        out.writeObject(code);
        out.writeObject(image);
        out.writeObject(description);
        out.writeObject(status);
        out.writeObject(sort);
        out.writeObject(createTime);
        out.writeObject(updateTime);
        out.writeObject(createUser);
        out.writeObject(updateUser);
        out.writeObject(isDeleted);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        id = (Long) in.readObject();
        name = (String) in.readObject();
        categoryId = (Long) in.readObject();
        price = (BigDecimal) in.readObject();
        code = (String) in.readObject();
        image = (String) in.readObject();
        description = (String) in.readObject();
        status = (Integer) in.readObject();
        sort = (Integer) in.readObject();
        createTime = (LocalDateTime) in.readObject();
        updateTime = (LocalDateTime) in.readObject();
        createUser = (Long) in.readObject();
        updateUser = (Long) in.readObject();
        isDeleted = (Integer) in.readObject();
    }

}
