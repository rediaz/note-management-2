package com.mannor.reggie_take_out.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mannor.reggie_take_out.common.R;
import com.mannor.reggie_take_out.entity.ShoppingCart;

import java.util.List;

public interface ShoppingCartService extends IService<ShoppingCart> {
//    R<ShoppingCart> addDishOrSetmeal(ShoppingCart shoppingCart);

    R<ShoppingCart> subDishOrSetmeal(ShoppingCart shoppingCart);

    void clean();
}
