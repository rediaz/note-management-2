package com.mannor.reggie_take_out.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mannor.reggie_take_out.common.CustomException;
import com.mannor.reggie_take_out.dto.SetmealDto;
import com.mannor.reggie_take_out.entity.Setmeal;
import com.mannor.reggie_take_out.entity.SetmealDish;
import com.mannor.reggie_take_out.mapper.SetmealMapper;
import com.mannor.reggie_take_out.service.SetmealDishService;
import com.mannor.reggie_take_out.service.SetmealService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SetmealServiceImpl extends ServiceImpl<SetmealMapper, Setmeal> implements SetmealService {

    @Autowired
    private SetmealDishService setmealDishService;

    @Autowired
    private SetmealService setmealService;


    /**
     * 添加套餐
     *
     * @param setmealDto
     */
    @Transactional
    @Override
    public void saveWithDish(SetmealDto setmealDto) {
        //添加数据到setmeal表
        this.save(setmealDto);

        //添加数据到setmealDish表
        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        setmealDishes = setmealDishes.stream().map(item -> {
            item.setSetmealId(setmealDto.getId());
            return item;
        }).collect(Collectors.toList());
        setmealDishService.saveBatch(setmealDishes);
    }

    /**
     * 修改套餐时的页面回显
     *
     * @param id
     * @return
     */
    @Override
    public SetmealDto getByIdWithDish(Long id) {
        SetmealDto setmealDto = new SetmealDto();
        //对setmeal表的操作
        Setmeal setmeal = this.getById(id);
        BeanUtils.copyProperties(setmeal, setmealDto);

        //对setmealDish表的操作
        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SetmealDish::getSetmealId, id);
        List<SetmealDish> list = setmealDishService.list(queryWrapper);

        setmealDto.setSetmealDishes(list);

        return setmealDto;
    }

    /**
     * 批量删除套餐
     *
     * @param ids
     */
    @Transactional
    @Override
    public void removeByIdsWithDish(Long ids[]) {
        //查询当前状态，看是否是可以删除的
        LambdaQueryWrapper<Setmeal> qw = new LambdaQueryWrapper<>();
        qw.eq(Setmeal::getStatus, 1).in(Setmeal::getId, ids);
        int count = this.setmealService.count(qw);
        if (count > 0) {
            //不能删除就抛出一个业务异常
            throw new CustomException("正在售卖中，不可以删除！");
        }

        //对表中数据进行删除
        List<Long> idList = Arrays.stream(ids).collect(Collectors.toList());
        this.removeByIds(idList);//对setmeal表的操作
        LambdaQueryWrapper<SetmealDish> lambdaQueryWrapper=new LambdaQueryWrapper<>();
        lambdaQueryWrapper.in(SetmealDish::getSetmealId,ids);
        setmealDishService.remove(lambdaQueryWrapper);//删除setmealDish表的数据



      /*  //下面的代码较赘余，所以改进后如上
        for (Long setmealId : ids) {
            setmealService.removeById(setmealId);//删除setmeal表中数据
            LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(SetmealDish::getSetmealId, setmealId);
            List<SetmealDish> setmealDishList = setmealDishService.list(queryWrapper);
            setmealDishList.stream().map(item -> {
                Long id = item.getId();
                setmealDishService.removeById(id);//删除setmealDish表中数据
                return item;
            });
        }*/


    }
}
